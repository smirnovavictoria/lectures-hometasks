/*
План лекции
1) Типы переменных infinity, undefined, Nan, null
2) Функции. Второй вариант как задать функцию
3) Связь HTML JS. Прослушивание
4) Добавление и удаление Li Ul
5) Анализ как делать домашние задания
*/

/* 
Функция — это "подпрограмма", которую можно вызывать. Функция состоит из последовательности инструкций, называемой телом функции.
 Значения могут быть переданы в функцию, а функция вернёт значение.
В JavaScript функции — это обьекты Function.
/*
// var t - декларация переменной, t = 0 - инициализация переменной

/*
Объяснение, почему NaN != Nan (Not a number)

(-1)^0 * 10.25 * 10^0
(-1)^0 * 1.025 * 10^1
(-1)^0 * 0.1025 * 10^2
(-1)^0 * 0.0010125 * 20^4
(-1)^0 * 102.5 * 10^-1
(-1)^0 * 1025 * 10^-2

1  11111111 1 00000000 00010110 00000000 01000000 0001
*/

// undefined null 0 false - все эти значение будут преобразованы в false

// infinity - значение бесконечности, может быть отрицательным

var a = null + "ddd";
console.log(a);

a = (null + true)/0;
console.log(a);

a = (null + true)/0.1;
console.log(a);

a = 5 + parseInt("10");
console.log(a);

a = 3;
let r = null + true - ++a - a + (false/Infinity) + a + true;
console.log(r);
//1-4-4+0+4+1

r = null + true - ++a - a + (false/Infinity) + a + true + NaN;
console.log(r);

function f(){
    t++;
}

var t = 9;
console.log(t);
f(); //переменная t изменит свое значение в том месте, где будет вызвана функция ее преобразования
console.log(t);

//Стрелочная функция

let x = () => 2;

/*
тоже самое, что и запись выше
let x = function(){
    return 2;
};
*/
 
let y = function(){
    return 2;
};

let y2 = function(){
    return function(){
        return 2;
    }
}
y2()(); //чтобы вызвать функцию внутри функии, пишем второй раз скобки

//алалог стрелочной

y = () => () => 2;

function f1(a, b){
    return a + b;
}

x = (a, b) => a+b;

console.log(x(3, 5));

x = (a, b) => ({a: a + b});

console.log(x(3,5));

x = (a, b) => ({a: a + b()});

console.log(x(3, () => 4));

console.log( x(3, () => ((a) => 4 + a())(()=>4)) );

function fx(a, b){
    return {
        a: a + b(),
    };
}

var res = fx(3, function(){
    return (function (a){
        return 4 + a();
    })(function(){
        return 4;
    });
});

console.log(res);