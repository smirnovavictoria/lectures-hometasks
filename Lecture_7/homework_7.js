"use strict"

let students = {
    st1: {
        name: "Vanya",
        estimate: 4.2,
        course: 10,
        active: true,
    },
    st2: {
        name: "Alex",
        estimate: 5,
        course: 11,
        active: false,
    },
    st3: {
        name: "Vova",
        estimate: 2,
        course: 2,
        active: false,
    },
    st4: {
        name: "Victoria",
        estimate: 3.2,
        course: 5,
        active: true,
    },
    st5: {
        name: "Viktor",
        estimate: 4.2,
        course: 15,
        active: true,
    },
    st6: {
        name: "Pavel",
        estimate: 3.3,
        course: 1,
        active: false,
    },
    st7: {
        name: "Bogdan",
        estimate: 4.4,
        course: 100,
        active: true,
    },
    st8: {
        name: "Julia",
        estimate: 3.1,
        course: 3,
        active: false,
    },
    st9: {
        name: "Artem",
        estimate: 4.9,
        course: 2,
        active: true,
    },
    st10: {
        name: "Georgiy",
        estimate: 2.5,
        course: 5,
        active: false,
    },
    st11: {
        name: "Andrey",
        estimate: 4.4,
        course: 100,
        active: true,
    },
    st12: {
        name: "Kate",
        estimate: 3.6,
        course: 3,
        active: false,
    },
    st13: {
        name: "Natali",
        estimate: 4.8,
        course: 1,
        active: true,
    },
    st14: {
        name: "Svetlana",
        estimate: 4.9,
        course: 3,
        active: false,
    },
    st15: {
        name: "Yra",
        estimate: 1.5,
        course: 2,
        active: false,
    },
    st16: {
        name: "Leo",
        estimate: 3,
        course: 4,
        active: true,
    },
    st17: {
        name: "Sergey",
        estimate: 3.9,
        course: 4,
        active: false,
    },
    st18: {
        name: "Maria",
        estimate: 4.3,
        course: 5,
        active: true,
    },
    st19: {
        name: "Alena",
        estimate: 4.4,
        course: 1,
        active: false,
    },
    st20: {
        name: "Olga",
        estimate: 4.5,
        course: 2,
        active: true,
    },
}

var j = 1; // для записи ключей новых студентов из задания 6.5, каждый ключ будет уникальный, но не будет соответствовать порядку первоначальных ключей st1,st2...
function add(students) { //функция добавления студентов в таблицу 

    let zag1 = document.createElement("h2"); //создаю переменную, которой присваиваю тег р
    zag1.innerHTML = "Вывод студентов"; //записываю в переменную строку
    document.querySelector("#tableStudents").append(zag1); //указываю, куда записать переменную в html

    var table = document.createElement("table"); //создаю переменную для записи в нее тега таблицы
    table.innerHTML = "<tr><th>Status<th>Name<th>Estimate<th>Cource<th>Deletion"; // добавляю первую строку в таблицу
    document.querySelector("#tableStudents").append(table); //определяю куда буду записывать таблицу в html

    var numLine = 1;
    for (let i in students) { //бегаю по объекту со студентами, с каждым новым циклом добавляю новую строку со след-им студентом

        var cell = document.createElement("td");
        let img = document.createElement("img");
        if (students[i].active == true) {
            img.src = "img/True.png";
            cell.append(img);
        } else {
            img.src = "img/False.png";
            cell.append(img);
        }

        var line = document.createElement("tr"); // присваиваю создание тега строки
        var cell1 = document.createElement("td"); // присваиваю 

        cell1.test = numLine;
        cell1.innerHTML = students[i].name; // записываю внутрь ячейки имя студента

        var cell2 = document.createElement("td"); //создаю ячейки в строку для описания студента
        cell2.test = numLine;
        cell2.innerHTML = students[i].estimate;

        var cell3 = document.createElement("td");
        cell3.test = numLine;
        cell3.innerHTML = students[i].course;

        var cell5 = document.createElement("td");
        cell5.innerHTML = '<input type = "checkbox" id="deletion" name="deletion">'; // создаю галочку для удаления студента из объекта

        if (students[i].estimate <= 3) {
            line.classList.add("losers");
        } else if (students[i].estimate > 3 && students[i].estimate <= 4) {
            line.classList.add("normal");
        } else if (students[i].estimate > 4) {
            line.classList.add("successful");
        }

        line.append(cell, cell1, cell2, cell3, cell5); // записываю ячейки в строку
        document.querySelector("tbody").append(line); // записываю строку в тела таблицы

        //меняет значок активности при нажатии
        cell.addEventListener("click", function () {
            if (students[i].active == true) {
                students[i].active = false;
            } else {
                students[i].active = true;
            }
            document.querySelector("#tableStudents").innerHTML = ""; // очищаем блок, в котором находится таблица, чтобы не было дублирования таблицы
            add(students); //запускаю заново функцию добавления студентов в таблицу
            document.querySelector("#tableStatistic").innerHTML = "";
            addStatistics(students);
        }
        );
        //удаляет студента если поставить галочку
        cell5.addEventListener("click", function () { // медот проверяет ячейку в которой расположена галочка на клик, если он был, выполняется функция
            delete students[i]; //удаляется студент, где i это имя ключа 
            document.querySelector("#tableStudents").innerHTML = ""; // очищаем блок, в котором находится таблица, чтобы не было дублирования таблицы
            add(students); //запускаю заново функцию добавления студентов в таблицу
            document.querySelector("#tableStatistic").innerHTML = "";
            addStatistics(students);
        }
        );

        //Появление текстового поля при нажатии и запись новых данных в объект
        var n = document.querySelector("#tableStudents").querySelector("tbody").childNodes;
        cell1.addEventListener("click", function (numLine) { // медот проверяет ячейку в которой расположена галочка на клик, если он был, выполняется функция
            n[this.test].childNodes[1].innerHTML = '<input type= "text" id="nameNew" name="name" value="" placeholder = "Name">';
            n[this.test].childNodes[1].firstChild.focus();
            document.querySelector("#nameNew").addEventListener("keydown", function (e) {
                if (e.keyCode === 13) {
                    students[i].name = document.querySelector("#nameNew").value;
                    document.querySelector("#tableStudents").innerHTML = ""; // очищаем блок, в котором находится таблица, чтобы не было дублирования таблицы
                    add(students);
                }
            });

            document.querySelector("#nameNew").addEventListener("blur", function () {
                document.querySelector("#nameNew").parentNode.innerHTML = students[i].name;
            });
        });

        cell2.addEventListener("click", function (numLine) { // медот проверяет ячейку в которой расположена галочка на клик, если он был, выполняется функция
            n[this.test].childNodes[2].innerHTML = '<input type= "text" id="estimateNew" name="estimate" value="" placeholder = "Estimate">'
            n[this.test].childNodes[2].firstChild.focus();
            document.querySelector("#estimateNew").addEventListener("keydown", function (e) {
                if (e.keyCode === 13) {
                    students[i].estimate = document.querySelector("#estimateNew").value;
                    document.querySelector("#tableStudents").innerHTML = ""; // очищаем блок, в котором находится таблица, чтобы не было дублирования таблицы
                    add(students);
                    document.querySelector("#tableStatistic").innerHTML = "";
                    addStatistics(students);
                }
            });

            document.querySelector("#estimateNew").addEventListener("blur", function () {
                document.querySelector("#estimateNew").parentNode.innerHTML = students[i].estimate;
            });
        });

        cell3.addEventListener("click", function (numLine) { // медот проверяет ячейку в которой расположена галочка на клик, если он был, выполняется функция
            n[this.test].childNodes[3].innerHTML = '<input type= "text" id="courseNew" name="course" value="" placeholder = "Course">'
            n[this.test].childNodes[3].firstChild.focus();
            document.querySelector("#courseNew").addEventListener("keydown", function (e) {
                if (e.keyCode === 13) {
                    students[i].course = document.querySelector("#courseNew").value;
                    document.querySelector("#tableStudents").innerHTML = ""; // очищаем блок, в котором находится таблица, чтобы не было дублирования таблицы
                    add(students);
                    document.querySelector("#tableStatistic").innerHTML = "";
                    addStatistics(students);
                }
            });

            document.querySelector("#courseNew").addEventListener("blur", function () {
                document.querySelector("#courseNew").parentNode.innerHTML = students[i].course;
            });
        });
        numLine++;
    }

    if (Object.keys(students).length == 0) {  //тут я проверяю длину массива, если она равна нулю, выполняю действие
        document.querySelector("#tableStudents").innerHTML = "";
        var pp = document.createElement("p"); //создаю переменную, которой присваиваю тег р
        pp.classList.add("pp");
        pp.innerHTML = "Студенты не найдены"; //записываю в переменную строку
        document.querySelector("#tableStudents").append(pp); //указываю, куда записать переменную в html
    }

}

add(students);

function addStatistics() {

    let heading = document.createElement("h2"); //создаю переменную, которой присваиваю тег р
    heading.innerHTML = "Статистика студентов"; //записываю в переменную строку
    document.querySelector("#tableStatistic").append(heading); //указываю, куда записать переменную в html

    var studentsActiveSr = {}; //создаем объект, который выаедет нам ср. оценку в разрезе каждого курса
    var inactiveStudents = {};
    var maxCourse = 1;

    for (let i in students) {
        if (students[i].course > maxCourse) {
            maxCourse = students[i].course;
        }
    }

    for (let i = 1; i <= maxCourse; i++) {
        var sum = 0;
        var sr = 0;
        var kolStud = 0;
        var kolStadInnective = 0;

        for (let j in students) {

            if (students[j].active == true && students[j].course == i) { //проверяет на активность студента и его принадлежность к определенному курсу
                kolStud++; //каждый шаг увеличиваем кол-во студентов на 1
                sum += students[j].estimate; //каждый шаг плюсуем к сумме след-ю оценку студент
            }
            if (students[j].active == false && students[j].course == i) {
                kolStadInnective++;
            }
        }

        if (sum !== 0) {
            sr = parseInt(sum / kolStud * 100) / 100; //считаем ср. оценку студента и округляем до сотых
            studentsActiveSr["Course " + i] = sr; //записываем значение средней оценки в объект 
        }

        if (kolStadInnective !== 0) {
            inactiveStudents["Course " + i] = kolStadInnective;
        }

    }

    var tableStatistic = document.createElement("table"); // создаю переменную и присваиваю ей тег таблицы
    document.querySelector("#tableStatistic").append(tableStatistic); //определяю куда будет записана таблица в html

    var line = document.createElement("tr");
    line.innerHTML = "<td>Курсы";

    var line2 = document.createElement("tr");
    line2.innerHTML = "<td>Средние оценки";

    var line3 = document.createElement("tr");
    line3.innerHTML = "<td>Курсы";

    var line4 = document.createElement("tr");
    line4.innerHTML = "<td>Неактивные студенты";
    tableStatistic.append(line, line2, line3, line4);

    for (let i in studentsActiveSr) {
        let cell = document.createElement("td");
        cell.innerHTML = i;
        line.append(cell);
        cell = document.createElement("td");
        if (studentsActiveSr[i] <= 3) {
            cell.classList.add("losers");
        } else if (studentsActiveSr[i] > 3 && studentsActiveSr[i] <= 4) {
            cell.classList.add("normal");
        } else if (studentsActiveSr[i] > 4) {
            cell.classList.add("successful");
        }
        cell.innerHTML = studentsActiveSr[i];
        line2.append(cell);
    }
    console.log(inactiveStudents);
    for (let i in inactiveStudents) {
        let cell = document.createElement("td");
        cell.innerHTML = i;
        line3.append(cell);
        cell = document.createElement("td");
        cell.innerHTML = inactiveStudents[i];
        line4.append(cell);
    }

    if (Object.keys(students).length == 0) {  //тут я проверяю длину массива, если она равна нулю, выполняю действие
        document.querySelector("#tableStatistic").innerHTML = ""; //обнуляю таблицу, чтобы пропала первая строка с заголовками
    }

    return {
        studentsActiveSr: studentsActiveSr,
        inactiveStudents: inactiveStudents
    }
}

addStatistics();

var form = document.createElement("div"); //создаю переменную в которую вкладываю блок, далее в блоке я создаю форму для добавления студента
form.innerHTML = '<form action="">' +
    '<input type="text" id="name" name="name" value="Name"> ' +
    '<input type="text" id="estimate" name="estimate" value="Estimate"> ' +
    '<input type="text" id="course" name="Course" value="Course"> ' +
    '<input type="checkbox" id="activity" name="activity" checked> ' +
    '<label for="activity">Student activity</label> ' +
    '<input type="button" id="submit" value="Добавить">' +
    '</form>';
document.querySelector("#formStudent").append(form); //определяю куда будет записана форма в html документе

document.querySelector("#submit").addEventListener("click", function () {  //обращаемся к кнопке через айди и прослушиваем ее на клик

    var stN = {}; //создаю пустой объект, для записи в него описания объекта (имя, курс, оценка, активность)
    stN.name = document.querySelector("#name").value; // записываю в объект ключ name и в него записываю значение из формы по айди
    stN.estimate = Number(document.querySelector("#estimate").value); //тоже самое по аналогии. пишу .value чтобы брать значение, а не ссылку на это значение
    stN.course = document.querySelector("#course").value;

    if (document.querySelector("#activity").checked == true) { // для записи активности, нужно определить какое значение тру или фолс у студента, для этого мы используем checked, который проверяет стоит ли галочка в чекбоксе
        stN.active = true; //если галочка стоит, тогда присваиваем тру
    } else {
        stN.active = false; //если не стоит, тогда фолс
    }

    students["stN" + j++] = stN; //записываем объект нового студента в наш общий объект студентов, для уникальности ключа с каждым шагом меняем переменну j
    console.log(students)
    document.querySelector("#tableStudents").innerHTML = ""; // обнуляем таблицу после добавления студента, иначе она будет дублироваться
    document.querySelector("#tableStatistic").innerHTML = "";
    add(students); //запускаем снова функцию добавления студентов из объекта в стаблицу
    addStatistics(students); //запускаю функцию, которая посчитает ср оценки и неактивных студентов из обновленного объекта студентов
});

/*
При каждом действии удаления или добавления студентов нужно пересчитывать статистику средней оценки в разрезе каждого курса
и подсчета количества неактивных студентов и изменять соответствующее содержимое.
Выполнение
Обнуляю тяблицу состактистикой в строке 312
запускаю функцию подсчета и вывода статистики исходя из обновленного объекта студентов 314
при повторном запуске функции addStatistics обнуляю объекты в которые записываю ср оценку и кол-во неактивных студентов, чтобы записать в таблицу обновленные данные
*/

/*
В ряде предыдущих заданий - выделять красным цветом тех студентов которые имеют оценку 3 и менее. которые от 3 до 4  - желтым
и которые 4 и выше - зеленым.
выполение в строке 155 и описание класса в файле homework_7.css
*/

/*
Аналогично как в предыдущем задании этого урока отмечать фоновым цветом вывод статистики в разрезе каждого курса
касательно средней оценки
выполение строка 248
*/

/*
Добавить для каждого студента иконку по нажатию на которую студент переводится в статус неактивный из активного и наоборот -
при этом для двух состояний иконки тоже должны быть разными и изменять
выполнение строка 140,183
*/

/*
По нажатие на имя студента - удалять имя, вместо имени показывать форму ввода - по нажатию на ENTER сохранять новое имя для этого студента,
 удалять форму ввода и выводить в списке новое имя студента
выполнение 206
*/

/*
По аналогии предыдущего пункта сделать тоже самое с номером курса и с оценкой студента.
 Не забыть что при изменении оценки статистика также должна быть пересчитана и выведена новая статистика.
 выполнение 206
*/
