"use strict"
/*
Задан двумерный массив - объединить каждый внутренний массив с верхнем массивом -
 только по уникальным значениям. Например [1,2,4[8,4,12,],[13,29,11],[0,5,3,11],5,6,7,[3,8,21],3],
  получаем в результате: [1,2,4,8,12,13,29,11,0,5,3,11,6,7,21]
*/

var mas = [2, 5, 6, [1, 2, 3], 5, [5, 9, 11], 7, 8, 22, [23, 24, 26, ["slovo", 111], 26], 10, 13, 14, 10, 11, 10, "slovo", "slovo"];
var mas2 = [1, 2, 3, 4, [5, 6, 7, [8, 9, 10], 11], 12, 13];
var mas3 = [1, 2, 3, 4, [5, 6, 7, [8, 9, 10], 11], 12, 13, 13];
var masUnik = [];
masUnik[0] = mas[0];

function perebor(mas) {

    for (let i in mas) {
        if (typeof mas[i] == "object") {
            perebor(mas[i]);
        } else {
            var sovpadenie = 0;

            for (let j in masUnik) {
                if (mas[i] == masUnik[j]) {
                    sovpadenie++;
                }
            }
            if (sovpadenie == 0) {
                masUnik.push(mas[i]);
            }

        }
    }
    return masUnik;
}

perebor(mas);
console.log(masUnik);

/*
Написать функцию которая возвращает true/false в зависимости от того - 
все ли уникальные значения в массиве или есть не уникальные
*/

var mas = [2, 5, 6, [1, 2, 3], 5, [5, 9, 11], 7, 8, 22, [23, 24, 26, ["slovo", 111], 26], 10, 13, 14, 10, 11, 10, "slovo", "slovo"];
var mas2 = [1, 2, 3, 4, [5, 6, 7, [8, 9, 10], 11], 12, 13];
var mas3 = [1, 2, 3, 4, [5, 6, 7, [8, 9, 10], 11], 12, 13, 13];
var masUnik = [];
masUnik[0] = mas[0];
var unikalnost = true;
function f(mas) {

    for (let i in mas) {
        if (typeof mas[i] == "object") {
            f(mas[i]);
        } else {
            var sovpadenie = 0;

            for (let j in masUnik) {
                if (mas[i] == masUnik[j]) {
                    sovpadenie++;
                    unikalnost = false;
                    return unikalnost;
                }
            }
            if (sovpadenie == 0) {
                masUnik.push(mas[i]);

            }

        }
    }
    return unikalnost;
}
f(mas);
console.log(unikalnost);

/*
Задан массив объектов студентов вида [{name: “Ivan”, estimate: 4, course: 1, active: true},
{name: “Ivan”, estimate: 3, course: 1, active: true},{name: “Ivan”, estimate: 2, course: 4, active: false},
{name: “Ivan”, estimate: 5, course: 2, active: true}] -
заполнить его более большим количеством студентов.
Написать функцию которая возвращает: среднюю оценку студентов в разрезе каждого курса:
{1: 3.2, 2: 3.5, 3: 4.5, 4: 3, 5: 4.5} с учетом только тех студентов которые активны.
Посчитать количество неактивных студентов в разрезе каждого курса и общее количество неактивных студентов.
*/

var students = [
    {
        name: "Vanya",
        estimate: 4.2,
        course: 2,
        active: true,
    },
    {
        name: "Alex",
        estimate: 5,
        course: 1,
        active: false,
    },
    {
        name: "Vova",
        estimate: 2,
        course: 2,
        active: false,
    },
    {
        name: "Victoria",
        estimate: 3.2,
        course: 5,
        active: true,
    },
    {
        name: "Viktor",
        estimate: 4.2,
        course: 4,
        active: true,
    },
    {
        name: "Pavel",
        estimate: 3.3,
        course: 1,
        active: false,
    },
    {
        name: "Bogdan",
        estimate: 4.4,
        course: 2,
        active: true,
    },
    {
        name: "Julia",
        estimate: 3.1,
        course: 3,
        active: false,
    },
    {
        name: "Artem",
        estimate: 4.9,
        course: 2,
        active: true,
    },
    {
        name: "Georgiy",
        estimate: 2.5,
        course: 5,
        active: false,
    },
    {
        name: "Andrey",
        estimate: 3.1,
        course: 5,
        active: true,
    },
    {
        name: "Kate",
        estimate: 3.6,
        course: 3,
        active: false,
    },
    {
        name: "Natali",
        estimate: 4.8,
        course: 1,
        active: true,
    },
    {
        name: "Svetlana",
        estimate: 4.9,
        course: 3,
        active: false,
    },
    {
        name: "Yra",
        estimate: 1.5,
        course: 2,
        active: false,
    },
    {
        name: "Leo",
        estimate: 3,
        course: 100,
        active: false,
    },
    {
        name: "Sergey",
        estimate: 3.9,
        course: 4,
        active: false,
    },
    {
        name: "Maria",
        estimate: 4.3,
        course: 5,
        active: true,
    },
    {
        name: "Alena",
        estimate: 4.4,
        course: 1,
        active: false,
    },
    {
        name: "Olga",
        estimate: 4.5,
        course: 15,
        active: true,
    }];//создаем массив всех студентов

var studentsActiveSr = {}; //создаем объект, который выаедет нам ср. оценку в разрезе каждого курса
var inactiveStudents = {};
var maxCourse = 1;

function avgEstimate() {

    for (let i in students) {
        if (students[i].course > maxCourse) {
            maxCourse = students[i].course;
        }
    }

    for (let i = 1; i <= maxCourse; i++) {
        var sum = 0;
        var sr = 0;
        var kolStud = 0;
        var kolStadInnective = 0;

        for (let j in students) {
            if (students[j].active == true && students[j].course == i) { //проверяет на активность студента и его принадлежность к определенному курсу

                kolStud++; //каждый шаг увеличиваем кол-во студентов на 1
                sum += students[j].estimate; //каждый шаг плюсуем к сумме след-ю оценку студент
            }
            if (students[j].active == false && students[j].course == i) {
                kolStadInnective++;
            }
        }
        sr = parseInt(sum / kolStud * 100) / 100; //считаем ср. оценку студента и округляем до сотых

        if (sum !== 0) {
            studentsActiveSr["Course " + i] = sr; //записываем значение средней оценки в объект 
        }
        if (kolStadInnective !== 0) {
            inactiveStudents["Course " + i] = kolStadInnective;
        }
    }
    return {
        studentsActiveSr: studentsActiveSr,
        inactiveStudents: inactiveStudents
    }
}

console.log(avgEstimate());


