var mas = [1, 2 , 3, 4];
console.log(mas[3]); //выведем значение по индексу 3

var mas1 = [1, 2 , 3, [1, 2, 3, 4]];
console.log(mas1 [3]); //выводит весь массив в массиве по этому индексу

var mas2 = [[1, 2, 3], [2, 3, 4, 5], [1, 2, 3, 4]];
//delete mas2[0];//удаляет весь массив под индексом 0
for (var i = 0; i < mas2.length; i++){  
    mas2[i].splice(0,2);//удаляет 2 значения по индексу 0
    //mas2[i].splice(i,2); будет удалять последовательно, т.к. i будет менятся 
}
console.log(mas2);


var a = 8;

function f(){
    a = 10;
    a++; 
}

function f2(){
    var a = 9; //тут мы переменную а декларируем, функция проверяет если ли а в f2(), еси нет, проверяет выше
}

function f3(a){
    var a = 9; // если пишем var, тогда переменную видим только в рамках функции f2()
    a++;
    function f4(){
        a++;
    }
    f4();
}

//f();//выведет  11
//f2();//выедет 8
f3();//выедет 8
console.log(a); 
/* 
Зона видимости
{
    a:8,
    {
        a:11,
        {
            a:9
            link: {
                a:11 //область замыкания!!!любая функция является замыканием
            }
            link:
        }
    }
    link: null //глобальная область видимости так же имеет ссылку, но ей уже некуда обращатся выше
}
*/
var obj = { //объект obj находится в объекте window (все что мы пишем в js находится в объекте window)
    a: 5
};

window.obj.a++;
console.log(window.obj.a);

var obj = { //объект obj находится в объекте window (все что мы пишем в js находится в объекте window)
    a: 5
};

function f(){
    return {
        b: 7,
        c: {
            d: 10,
        }
    }
};

var a = window.f();
console.log(window.a.b);
console.log(window.a.c.d);

/*
Объект Math является встроенным объектом, 
хранящим в своих свойствах и методах различные математические константы и функции. 
Объект Math не является функциональным объектом.
*/
console.log(Math.ceil(23.09)); //округление к большему

var s = "2.34";
var s2 = "2.06";

console.log(parseInt(s) + parseInt(s2)); //убрало дробную часть и целое присвоила к переменной

console.log(Math.floor(23.09)); //округляет к ближайшему целому
console.log(Math.floor(23.9)); //если дробная часть меньше 5, округляет к меньшему целому, если больше 5, то к большему целому (как в математике_)

var r = 10;
var s = Math.PI * Math.pow(r, 2); // считает r в квадрате

console.log(s);

var s = Math.sqrt(Math.pow(r, 2)); // 

console.log(s);

/*
forEach(function(element, index, array){})- бегает по элементам массива
push - добавляет в конец
pop - удаляет последний и возвращает его
reverse - переворачивает массив
shift - удаляет первый и возвращает его
find(function(element, index, ar){}); - Находит первый элемент
*/
var m = [];

m.push(1); //добавляет в конец
m.push(2);
m.push(3);
m.push(4);

console.log(m.length - 1);// показывает последнее значение
console.log(m);

console.log(m.pop());//удаляет последний и возвращает его
console.log(m);
console.log(m.reverse());//переворачивает массив
console.log(m.shift());//удаляет первый и возвращает его

//найти минимальное число в массиве
var m = [3,4,-7,-1,5,-2,8];
/*
var res = 0;
for(var i = 0; i < m.length; i++){
    if(m[i] < 0){
        res = m[i];
        break;
    }
}
console.log(res);
*/

function search(el, index, arr){
    console.log("Index: " + index + "-" + "Elevent: " + el);
    //return el < 0;
}

m.find(search); //find это как цикл бегающий по элементам массива и для каждого эл. массива он вызывает функцию search
console.log(m);

function search1(el, index, arr){
    console.log("Index: " + index + "-" + "Elevent: " + el);
    return true;
    //return el < 0;
}

m.find(search1); //find это как цикл бегающий по элементам массива и для каждого эл. массива он вызывает функцию search
console.log(m);

function search2(el, index, arr){//передало значения (3, 0, массив)
    return el < 0;// 3 < 0 false, доходим до начения -1 < 0 true
}

function find(m, callback){//передает m = массив, callback = search2
    for(var i = 0; i < m.length; i++){
        var res = callback(m[i], i, m);//вызывает функцию выше со значениями search2(3, 0, массив)
        if(res){//false,сюда не заходим, когда true заходим 
            return m[i];//возвращаем текущее значение
        }
    }
}
//find(m, search2); //find это как цикл бегающий по элементам массива и для каждого эл. массива он вызывает функцию search

var el = find(m, search2);
console.log(el);

var m1 = [3,4,-7,-1,5,-2,8];

function search3(el, index, arr){//передало значения (3, 0, массив)
    return el < 0;// 3 < 0 false, доходим до начения -1 < 0 true
}

var el = m.find(search3); //выше мы просто расписали как работает функция find
console.log(el);

/* Пример не работает
function search4(el, index, arr){
    return el < 0;
}
var m1 = [3,4,-7,-1,5,-2,8];

var element = m1.forEach(function(el, index, arr){
    return el < 0;
}); //forEach заменяет все циклы, но обычные циклы работают намного быстрее);
console.log(element);
*/

var s = "String";//строка это массив символов

var t = 9;//на любую переменную нужно одно и тоже кол-во памяти
t = 10; //строку нельзя перезаписать как переменную, потому что постоянно меняется выделенная память под нее

var h = "sd";
h = "sdfghhfd";// т.к. изначально было выделено на сроку меньше памяти и система не знает сколько потребует памяти в дальнейшем, она просто перезаписывает новое значение строки, предыдущую стирает
h = "a";//даже если последующие изменения требуют меньше памяти, система все равно перезапишет значение и возьмет памяти сколько нужно на новое значение

var s1 = "1";
var s2 = "a";

var s3 = s2 + s1;
console.log(s3);
 

var s = "Hello my firends";

var s2 = "my";

console.log(s.indexOf(s2));//начало, индекс с которого начинается строка s2


var s = "Hello my firends";

var s2 = "aaa";

console.log(s.indexOf(s2));//-1 если нет такой строки в s


var s = "Hello my firends";

var s2 = "aaa";

if(s.indexOf(s2) !== -1){
    console.log(1);
}else{
    console.log(2);//"aaa" нет в "Hello my firends", поэтому значение false -1, -1 != -1 не выполняется, поэтому вывод 2
}


var s = "Hello my firends";

var s2 = "firends";

if(s.indexOf(s2) !== -1){
    console.log(1);
}else{
    console.log(2);//firends начинается с индекса 6, 6 != -1, выполняется, тогда выводим 1
}


var a = 2;
console.log(~a); //-(a+1)
//~  инвертирование битов, меняет 0 на 1 и наоборот, если а положительное, то получаем отрицательное значение +(-1), если ф отрицательное, тогда на выходе положительное - 1

/*
00000000 00000000 00000000 00000010 
11111111 11111111 11111111 11111101
*/

var p = document.querySelector("p");
p.outerHTML = "";//делает строку пустой, т.е она пропадает со страницы

console.dir(p);
console.dir(p.innerHTML);//выводит то, что внутри объекта р

var p1 = document.querySelector("p1");
console.dir(p1);
var a = parseInt(p1.innerHTML);
var b = parseInt(p1.innerHTML);

var res = a + b;
p1.innerHTML = '<span class="inner">result: ' + res + '</span>';
console.dir(res);

var p = document.querySelectorAll("p");//выведет массивом все р
console.dir(p);

for(var i = 0; i < p.length; i++){
    var spans = p[i].querySelectorAll("span");
    console.log(spans.length);
}

var p = document.getElementById("p");//точно так же выводит, но только р, которые имеют ID
console.dir(p);

/*
Способы, с помощью которых мы можем получить элементы из HTML
getElementById
querySelector
SquerySelectorAll
*/
var p = document.getElementById("asd");
var num = p.getAttribute("data-number");
num *= 2;
p.setAttribute("data-number", num); //записали новое значение data-number
console.log(num);
