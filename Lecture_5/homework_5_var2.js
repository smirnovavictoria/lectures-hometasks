"use strict"
/*
Задан двумерный массив - объединить каждый внутренний массив с верхнем массивом -
 только по уникальным значениям. Например [1,2,4[8,4,12,],[13,29,11],[0,5,3,11],5,6,7,[3,8,21],3],
  получаем в результате: [1,2,4,8,12,13,29,11,0,5,3,11,6,7,21]
*/

//решение не работает
var mas = [2, 5, 6, [1, 2, 3], 5, [5, 9, 11], 7, 8, 22, [23, 24, 26, ["slovo", 111], 26], 10, 13, 14, 10, 11, 10, "slovo", "slovo"];
var mas2 = [1, 2, 3, 4, [5, 6, 7, [8, 9, 10], 11], 12, 13];
var mas3 = [1, 2, 3, 4, [5, 6, 7, [8, 9, 10], 11], 12, 13, 13];
var masUnik = [];
masUnik[0] = mas[0];

function search(element, index, array) { //бегает по массиву mas

    if (typeof (mas[index]) == "object") {//проверяет каждый элемент, является ли он массивом

        mas[index].find(search2);//если является массивом, тогжа запускается функция перебора внутреннего массива

        function search2(el, ind, arr) {//бегает по внутреннему массиву

            if (typeof mas[index][ind] == "object") {

                mas[index][ind].find(search4);

                function search4(num, j, m3) {//бегаем по внутреннему массиву предыдущего внутреннего массива

                    masUnik.find(search5);//вызываем функцию search5 для сравнения с уникальным массивом
                    var sovpadenie = 0;//вводим переменную sovpadenie для определения уникального значения
                    function search5(e, i, a) {//бегаем по уникальному массивую
                        if (mas[index][ind][j] == masUnik[i]) {//если значение внутреннего массива равно значению уникального массива, тогда у нас совпадение и мы не записываем новое значение
                            sovpadenie++;//меняем значение sovpadenie++, чтобы когда оно будет равно 0 мы записали новое значение в уникальный массив
                        }
                    }
                    if (sovpadenie == 0 && mas[index][ind][j] != "object") {
                        masUnik.push(mas[index][ind][j]);
                    }
                }
            } else {
                var sovpadenie = 0;//вводим переменную, с помощью которой будем решать записывать ли новое значение. она будет обнуляться с каждым повтором функции search3
                masUnik.find(search3);//запускаем функцию для перебора уникального массива
                //for(var j = 0; j < masUnik.length; j++)
                function search3(e, i, a) {//бегает по значениям уникального массива

                    if (mas[index][ind] == masUnik[i] && mas[index][ind] == "objects") {//если значение внутреннего массива равно значению уникального массива, тогда у нас совпадение и мы не записываем новое значение
                        sovpadenie++;//меняем значение sovpadenie++, чтобы когда оно будет равно 0 мы записали новое значение в уникальный массив
                    }
                }
                if (sovpadenie == 0) {
                    masUnik.push(mas[index][ind]);
                }
            }


        }

    }

    else {
        var sovpadenie = 0;
        masUnik.find(search2);
        function search2(el, ind, arr) {

            if (mas[index] == masUnik[ind]) {
                sovpadenie++;

            }
        }
        if (sovpadenie == 0) {
            masUnik.push(mas[index]);

        }

    }

}

mas.find(search);
console.log(masUnik);

/*
Задан массив объектов студентов вида [{name: “Ivan”, estimate: 4, course: 1, active: true},
{name: “Ivan”, estimate: 3, course: 1, active: true},{name: “Ivan”, estimate: 2, course: 4, active: false},
{name: “Ivan”, estimate: 5, course: 2, active: true}] -
заполнить его более большим количеством студентов.
Написать функцию которая возвращает: среднюю оценку студентов в разрезе каждого курса:
{1: 3.2, 2: 3.5, 3: 4.5, 4: 3, 5: 4.5} с учетом только тех студентов которые активны.
Посчитать количество неактивных студентов в разрезе каждого курса и общее количество неактивных студентов.
*/

var students = [
    {
        name: "Vanya",
        estimate: 4.2,
        course: 2,
        active: true,
    },
    {
        name: "Alex",
        estimate: 5,
        course: 1,
        active: false,
    },
    {
        name: "Vova",
        estimate: 2,
        course: 2,
        active: false,
    },
    {
        name: "Victoria",
        estimate: 3.2,
        course: 5,
        active: true,
    },
    {
        name: "Viktor",
        estimate: 4.2,
        course: 4,
        active: true,
    },
    {
        name: "Pavel",
        estimate: 3.3,
        course: 1,
        active: false,
    },
    {
        name: "Bogdan",
        estimate: 4.4,
        course: 2,
        active: true,
    },
    {
        name: "Julia",
        estimate: 3.1,
        course: 3,
        active: false,
    },
    {
        name: "Artem",
        estimate: 4.9,
        course: 2,
        active: true,
    },
    {
        name: "Georgiy",
        estimate: 2.5,
        course: 5,
        active: false,
    },
    {
        name: "Andrey",
        estimate: 3.1,
        course: 5,
        active: true,
    },
    {
        name: "Kate",
        estimate: 3.6,
        course: 3,
        active: false,
    },
    {
        name: "Natali",
        estimate: 4.8,
        course: 1,
        active: true,
    },
    {
        name: "Svetlana",
        estimate: 4.9,
        course: 3,
        active: false,
    },
    {
        name: "Yra",
        estimate: 1.5,
        course: 2,
        active: false,
    },
    {
        name: "Leo",
        estimate: 3,
        course: 5,
        active: false,
    },
    {
        name: "Sergey",
        estimate: 3.9,
        course: 4,
        active: false,
    },
    {
        name: "Maria",
        estimate: 4.3,
        course: 5,
        active: true,
    },
    {
        name: "Alena",
        estimate: 4.4,
        course: 1,
        active: false,
    },
    {
        name: "Olga",
        estimate: 4.5,
        course: 4,
        active: true,
    }];//создаем массив всех студентов 

//в данном решении функции ограничены, считают только до 5го курса

function avgEstimate() { //эта функция считает среднюю оценку студентов в разрезе каждого курса
    var sum1 = 0; //переменная считает сумму всех оценок студентов в разрезе каждого курса
    var sum2 = 0;
    var sum3 = 0;
    var sum4 = 0;
    var sum5 = 0;
    var sr = 0;
    var sr1 = 0; //средняя оценка студентов 1-го курса и т.д. 
    var sr2 = 0;
    var sr3 = 0;
    var sr4 = 0;
    var sr5 = 0;
    var kolstud1 = 0; //считает кол-во студентов на курсе
    var kolstud2 = 0;
    var kolstud3 = 0;
    var kolstud4 = 0;
    var kolstud5 = 0;
    var studentsActiveSr = {}; //создаем объект, который выаедет нам ср. оценку в разрезе каждого курса

    for (let i in students) {

        if (students[i].active == true) {

            switch (students[i].course) {

                case 1: sum1 += students[i].estimate; kolstud1++; break;
                case 2: sum2 += students[i].estimate; kolstud2++; break;
                case 3: sum3 += students[i].estimate; kolstud3++; break;
                case 4: sum4 += students[i].estimate; kolstud4++; break;
                case 5: sum5 += students[i].estimate; kolstud5++; break;
                default: sr = console.log("Incorrect course number");
            }
        }
    }
    sr1 = parseInt(sum1 / kolstud1 * 100) / 100;
    sr2 = parseInt(sum2 / kolstud2 * 100) / 100;
    sr3 = parseInt(sum3 / kolstud3 * 100) / 100;
    sr4 = parseInt(sum4 / kolstud4 * 100) / 100;
    sr5 = parseInt(sum5 / kolstud5 * 100) / 100;

    studentsActiveSr["Course " + 1] = sr1;
    studentsActiveSr["Course " + 2] = sr2;
    studentsActiveSr["Course " + 3] = sr3;
    studentsActiveSr["Course " + 4] = sr4;
    studentsActiveSr["Course " + 5] = sr5;

    return studentsActiveSr;
}

var inactiveStudents = {};
var kolStudCourse = 0;
var kolStud = 0;

function inactive() { //функция считает кол-во неактивный студентов на каждом курсе и общее их кол-во 
   
    kolStudCourse = 0; //обнуляем кол-во студентов для каждого курса

    for (let i in students) {
        if (students[i].active == false && students[i].course == 1) {
            kolStudCourse++;
            kolStud++; //обще кол-во студентов не обнуляем и увеличиваем на 1 в каждом следующем цикле
        }
    }
    inactiveStudents["Course " + 1] = kolStudCourse; //записываем в объект кол-во неактивных студентов для каждого курса

    kolStudCourse = 0;

    for (let i in students) {
        if (students[i].active == false && students[i].course == 2) {
            kolStudCourse++;
            kolStud++;
        }
    }
    inactiveStudents["Course " + 2] = kolStudCourse;

    kolStudCourse = 0;

    for (let i in students) {
        if (students[i].active == false && students[i].course == 3) {
            kolStudCourse++;
            kolStud++;
        }
    }
    inactiveStudents["Course " + 3] = kolStudCourse;

    kolStudCourse = 0;

    for (let i in students) {
        if (students[i].active == false && students[i].course == 4) {
            kolStudCourse++;
            kolStud++;
        }
    }
    inactiveStudents["Course " + 4] = kolStudCourse;

    kolStudCourse = 0;

    for (let i in students) {
        if (students[i].active == false && students[i].course == 5) {
            kolStudCourse++;
            kolStud++;
        }
    }
    inactiveStudents["Course " + 5] = kolStudCourse;
    inactiveStudents["Total inactive students"] = kolStud; //записываем в объект общее кол-во неактивных студентов
    return inactiveStudents; //функция возвращает заполненный нами объект с неактивными студентами
}

console.log(avgEstimate());
console.log(inactive());
