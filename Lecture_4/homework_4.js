"use strict"
/*
Написать функцию которая генерирует массив случайных значений, таким образом,
 что все элементы результирующего массива являются уникальными. 
 Генерациями происходит в рамках чисел от N до M, где N,M - могут быть как положительные так и отрицательными,
  и еще одним параметром количество значений которые нужно сгенерировать. 
  Если количество генерируемых значений больше чем чисел в диапазоне - отдавать пустой массив.
*/

function frandom(n, min, max) {
  if (n > (max + 1 - min)) {
    return m = [];
  }
  var m = [Math.floor(Math.random() * (max + 1 - min) + min)];
  for (var i = 1; i < n; i++) {
    (function genarete() {
      var testSovpadeniya = 0;
      var randomNum = Math.floor(Math.random() * (max + 1 - min) + min); //находим интервал всех значений, а потом сдвигаем влево или вправо в зависимости от заданных параметров
      for (var j = 0; j < m.length; j++) {
        if (m[j] == randomNum) {
          testSovpadeniya++;
        }
      }
      if (testSovpadeniya == 0) {
        m[j] = randomNum;
      } else {
        return genarete();
      }
    })();
  }
  return m;
}

console.log(frandom(20, 0, 10));

/*
Использовать функцию из предыдущего задания чтобы получить массив из нужного количества значений. 
Найти процентное соотношение отрицательных, положительных и нулевых элементов массива.
*/

function znacheniya(n, min, max) {
  var pozitiv = 0;
  var zero = 0;
  var negativ = 0;
  var mas = frandom(n, min, max);
  console.log(mas);
  for (var j = 0; j < mas.length; j++) {
    if (mas[j] > 0) {
      pozitiv++;
    }
    else if (mas[j] == 0) {
      zero++;
    } else {
      negativ++;
    }
  }
  var pozitivPercent = parseInt(pozitiv * 100 / n * 100) / 100;
  var zeroPercent = parseInt(zero * 100 / n * 100) / 100;
  var negativPercent = parseInt(negativ * 100 / n * 100) / 100;
  let percent = {
    pozitivPercent: pozitivPercent,
    zeroPercent: zeroPercent,
    negativPercent: negativPercent,
  }
  return percent;
}

console.log(znacheniya(9, -5, 4));

/*
Все предыдущий задания на циклы - написать с помощью циклов for in и/или for of
*/
// Переменная содержит в себе строку. Вывести строку в обратном порядке.
var a = "JavaScript";
var someText = [];
for (let i in a) {
  someText = a[i] + someText;
}
console.log(someText);

// Найти сумму цифр числа которые кратны двум

var num = 7894235687123462;
var res = 0;
var numStr = num + "";

for (let i in numStr) {
  if (numStr[i] % 2 == 0) {
    var preob = parseInt(numStr[i]);
    res += preob;
  }
}
console.log(res);

// Написать функцию, которая транспонирует матрицу

var mat = [
  [5, 1, 4, 2],
  [10, 6, 7, 5],
  [4, 2, 6, 1]
];
var revMas = [];
for (let i in mat) {

  for (let j in mat[i]) {
    if (i == 0) {
      revMas[j] = [];
    }
    revMas[j][i] = mat[i][j];
  }
}
console.log("Inverted array")
console.log(revMas);

// Написать функцию, которая складывает две матрицы

var mat1 = [
  [2, 2, 1, 1],
  [4, 4, 6, 4],
  [4, 8, 8, 9]
];
var mat2 = [
  [1, 2, 7, 1],
  [5, 6, 6, 5],
  [7, 9, 9, 10]
];
var matrSum = [];

for (let i in mat1) {
  matrSum[i] = [];
  for (let j in mat1[i]) {
    matrSum[i][j] = mat1[i][j] + mat2[i][j];
  }
}
console.log("Сумма 2 матриц");
console.log(matrSum);

// Найти номер столбца двумерного массива сумма которого является максимальной (аналогично для строки)

var sumRowMax = 0;
var sumColMax = 0;
var numRow = 0;
var numCol = 0;
var mat = [
  [10, 2, 4, 2, 7, 9, 10],
  [4, 6, 6, 5, 5, 10, 9],
  [7, 6, 4, 2, 5, 10, 9],
  [5, 6, 6, 10, 5, 10, 9],
  [0, 0, 0, 10, 5, 10, 100],
];

for (let i in mat) {
  var sumRow = 0;
  for (let j in mat[i]) {
    sumRow += mat[i][j]; //меняет индексы, потом массив, суммирует
  }
  if (sumRowMax < sumRow) {
    numRow = ++i;
    sumRowMax = sumRow;
  }
}

for (let colIndex in mat[0]) {
  var sumCol = 0;
  for (let rowIndex in mat) {
    sumCol += mat[rowIndex][colIndex]; //меняет массивы, потом индекс, суммирует
  }
  if (sumColMax < sumCol) {
    numCol = ++colIndex;
    sumColMax = sumCol;
  }
}
console.log(mat);
console.log("Row number = " + numRow);
console.log("Maximum sum of rows = " + sumRowMax);
console.log("Column number = " + numCol);
console.log("Maximum sum of columns = " + sumColMax);


/*Задано предложение - подсчитать количество вхождений каждого слова в предложении.
 Вывести список уникальных слов и напротив каждого слова - сколько раз встретилось
 */

var string = "Один, Два, Три, три, четыре, четыре, четыре, пять, шесть, шесть, шесть!";
var slovo = "";
var m = [];

for (var j = 0; j < string.length + 1; j++) {
  if (string[j] == " " || string[j] == undefined && slovo != " ") {
    if (slovo != "") {
      m.push(slovo.toLowerCase());
      slovo = "";
    }
  }
  if (string[j] != " " && string[j] != "!" && string[j] != "." && string[j] != ",") {
    slovo += string[j];
  }
}

for (var i = 0; i < m.length; i++) {

  var kolpovtor = 0;

  for (var j = 0; j < m.length; j++) {
    if (m[j] == m[i]) {
      kolpovtor++;
    }
  }
  var kolSovpadeniy = 0;
  for (var p = 0; p < i; p++) { //для вывода уникальных слов, а не всех подряд. 
    if (m[p] == m[i]) {
      kolSovpadeniy++;

    }
  }
  if (kolSovpadeniy == 0) { //если совпадение было, то значит это слово уже выводилось
    console.log(m[i] + " " + kolpovtor);
  }
}

/*
Написать рекурсивную функцию которая выводит абсолютно все элементы ассоциативного массива (объекта) - любого уровня вложенности
*/

let mas = {
  a: 3,
  b: {
    a: 2,
    b: 3,
  },
  d: {
    a: "4",
    dfg: "555",
    d: {
      a: 6,
      b: "77",
      c: {
        b: "8",
        a: [9, 10, [10, 10, 10], 11],
      },
    },
  },
};

function show(ob) {
  for (let i in ob) {
    if (typeof ob[i] === "object") {
      show(ob[i]);
    } else {
      console.log(ob[i]);
    }
  }
}

show(mas);

