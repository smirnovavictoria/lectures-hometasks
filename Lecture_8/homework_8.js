"use strict"

if (localStorage.getItem("Students") == null) {
    localStorage.setItem("Students", "{}");
}

var key = localStorage.getItem("key");
console.log(key);// для записи ключей новых студентов из задания 6.5, каждый ключ будет уникальный, но не будет соответствовать порядку первоначальных ключей st1,st2...
var students = JSON.parse(localStorage.getItem("Students"));

function add(students) { //функция добавления студентов в таблицу 

    let zag1 = document.createElement("h2"); //создаю переменную, которой присваиваю тег р
    zag1.innerHTML = "Вывод студентов"; //записываю в переменную строку
    document.querySelector("#tableStudents").append(zag1); //указываю, куда записать переменную в html

    var table = document.createElement("table"); //создаю переменную для записи в нее тега таблицы
    table.innerHTML = "<tr><th>Status<th>Name<th>Estimate<th>Cource<th>Email address<th>Time<th>Deletion"; // добавляю первую строку в таблицу
    document.querySelector("#tableStudents").append(table); //определяю куда буду записывать таблицу в html

    let newStudentsNumber = 0;

    var numLine = 1;
    for (let i in students) { //бегаю по объекту со студентами, с каждым новым циклом добавляю новую строку со след-им студентом

        var cell = document.createElement("td");
        let img = document.createElement("img");
        if (students[i].active == true) {
            img.src = "img/True.png";
            cell.append(img);
        } else {
            img.src = "img/False.png";
            cell.append(img);
        }

        let time = Date.now() / 100 / 60 / 60;

        if (students[i].date != undefined) {
            if ((time - students[i].date.allTime) < 60) {
                newStudentsNumber++;

            }
        }

        var line = document.createElement("tr"); // присваиваю создание тега строки
        var cell1 = document.createElement("td"); // присваиваю 

        cell1.test = numLine;
        cell1.innerHTML = students[i].name; // записываю внутрь ячейки имя студента

        var cell2 = document.createElement("td"); //создаю ячейки в строку для описания студента
        cell2.test = numLine;
        cell2.innerHTML = students[i].estimate;

        var cell3 = document.createElement("td");
        cell3.test = numLine;
        cell3.innerHTML = students[i].course;

        var cell4 = document.createElement("td");
        cell4.test = numLine;
        cell4.innerHTML = students[i].email;

        var cell6 = document.createElement("td");
        cell6.test = numLine;
        var minutes = students[i].date.minutes;
        var hours = students[i].date.hours;
        var months = students[i].date.month;

        if (students[i].date !== undefined) {

            if (students[i].date.minutes < 10) {

                minutes = "0" + students[i].date.minutes;
            }
            if (students[i].date.hours < 10) {

                hours = "0" + students[i].date.hours;

            }
            if (students[i].date.month < 10) {
                months = "0" + students[i].date.month;
            }
            cell6.innerHTML = students[i].date.day + "." + (months + 1) + "." + students[i].date.year + " " + hours + ":" + minutes;
        } else {
            cell6.innerHTML = "Time not set";
        }

        var cell5 = document.createElement("td");
        cell5.innerHTML = '<input type = "checkbox" id="deletion" name="deletion">'; // создаю галочку для удаления студента из объекта

        if (students[i].estimate <= 3) {
            line.classList.add("losers");
        } else if (students[i].estimate > 3 && students[i].estimate <= 4) {
            line.classList.add("normal");
        } else if (students[i].estimate > 4) {
            line.classList.add("successful");
        }

        line.append(cell, cell1, cell2, cell3, cell4, cell6, cell5); // записываю ячейки в строку
        document.querySelector("tbody").append(line); // записываю строку в тела таблицы

        //меняет значок активности при нажатии
        cell.addEventListener("click", function () {
            if (students[i].active == true) {
                students[i].active = false;
            } else {
                students[i].active = true;
            }

            localStorage.setItem("Students", JSON.stringify(students));
            document.querySelector("#formStudent").innerHTML = "";
            document.querySelector("#tableStudents").innerHTML = ""; // очищаем блок, в котором находится таблица, чтобы не было дублирования таблицы
            add(students); //запускаю заново функцию добавления студентов в таблицу
            document.querySelector("#tableStatistic").innerHTML = "";
            addStatistics(students);
        }
        );

        //удаляет студента если поставить галочку
        cell5.addEventListener("click", function () { // медот проверяет ячейку в которой расположена галочка на клик, если он был, выполняется функция
            delete students[i]; //удаляется студент, где i это имя ключа 
            localStorage.setItem("Students", JSON.stringify(students));
            document.querySelector("#formStudent").innerHTML = "";
            document.querySelector("#tableStudents").innerHTML = ""; // очищаем блок, в котором находится таблица, чтобы не было дублирования таблицы
            add(students); //запускаю заново функцию добавления студентов в таблицу
            document.querySelector("#tableStatistic").innerHTML = "";
            addStatistics(students);
        }
        );

        //Появление текстового поля при нажатии и запись новых данных в объект
        var n = document.querySelector("#tableStudents").querySelector("tbody").childNodes;
        cell1.addEventListener("click", function (numLine) { // медот проверяет ячейку в которой расположена галочка на клик, если он был, выполняется функция
            n[this.test].childNodes[1].innerHTML = '<input type= "text" id="nameNew" name="name" value="" placeholder = "Name">';
            n[this.test].childNodes[1].firstChild.focus();
            document.querySelector("#nameNew").addEventListener("keydown", function (e) {
                let checkName = /^[A-ZА-Я][a-zа-я]{1,14}$/.test(document.querySelector("#nameNew").value);

                if (e.keyCode === 13 && checkName) {
                    document.querySelector("#nameNew").value[0].toUpperCase();
                    students[i].name = document.querySelector("#nameNew").value;

                    localStorage.setItem("Students", JSON.stringify(students));
                    document.querySelector("#formStudent").innerHTML = "";
                    document.querySelector("#tableStudents").innerHTML = ""; // очищаем блок, в котором находится таблица, чтобы не было дублирования таблицы
                    add(students);
                } else if (e.keyCode === 13) {
                    alert("Enter the name correctly");
                }
            });

            document.querySelector("#nameNew").addEventListener("blur", function () {
                document.querySelector("#nameNew").parentNode.innerHTML = students[i].name;
            });
        });

        cell2.addEventListener("click", function (numLine) { // медот проверяет ячейку в которой расположена галочка на клик, если он был, выполняется функция
            n[this.test].childNodes[2].innerHTML = '<input type= "text" id="estimateNew" name="estimate" value="" placeholder = "Estimate">'
            n[this.test].childNodes[2].firstChild.focus();
            document.querySelector("#estimateNew").addEventListener("keydown", function (e) {
                if (e.keyCode === 13 && document.querySelector("#estimateNew").value <= 5 && document.querySelector("#estimateNew").value > 0) {
                    students[i].estimate = Number(document.querySelector("#estimateNew").value);
                    localStorage.setItem("Students", JSON.stringify(students));
                    document.querySelector("#formStudent").innerHTML = "";
                    document.querySelector("#tableStudents").innerHTML = ""; // очищаем блок, в котором находится таблица, чтобы не было дублирования таблицы
                    add(students);
                    document.querySelector("#tableStatistic").innerHTML = "";
                    addStatistics(students);
                } else if (e.keyCode === 13) {
                    alert("Enter the estimate 1 through 5!");
                }
            });

            document.querySelector("#estimateNew").addEventListener("blur", function () {
                document.querySelector("#estimateNew").parentNode.innerHTML = students[i].estimate;
            });
        });

        cell3.addEventListener("click", function (numLine) { // медот проверяет ячейку в которой расположена галочка на клик, если он был, выполняется функция
            n[this.test].childNodes[3].innerHTML = '<input type= "text" id="courseNew" name="course" value="" placeholder = "Course">'
            n[this.test].childNodes[3].firstChild.focus();
            document.querySelector("#courseNew").addEventListener("keydown", function (e) {
                let checkCoure = /^[1-5]$/.test(document.querySelector("#courseNew").value);
                if (e.keyCode === 13 && checkCoure) {
                    students[i].course = document.querySelector("#courseNew").value;
                    localStorage.setItem("Students", JSON.stringify(students));
                    document.querySelector("#formStudent").innerHTML = "";
                    document.querySelector("#tableStudents").innerHTML = ""; // очищаем блок, в котором находится таблица, чтобы не было дублирования таблицы
                    add(students);
                    document.querySelector("#tableStatistic").innerHTML = "";
                    addStatistics(students);
                } else if (e.keyCode === 13) {
                    alert("Enter course 1 through 5");
                }
            });

            document.querySelector("#courseNew").addEventListener("blur", function () {
                document.querySelector("#courseNew").parentNode.innerHTML = students[i].course;
            });
        });

        cell4.addEventListener("click", function (numLine) { // медот проверяет ячейку в которой расположена галочка на клик, если он был, выполняется функция
            n[this.test].childNodes[4].innerHTML = '<input type= "text" id="emailNew" name="email" value="" placeholder = "Email">'
            n[this.test].childNodes[4].firstChild.focus();

            document.querySelector("#emailNew").addEventListener("keydown", function (e) {
                let checkEmail = /^.{1,25}\@{1}[a-z]{1,9}(\.[a-z]{2,4}){1,2}$/.test(document.querySelector("#emailNew").value);
                if (e.keyCode === 13 && checkEmail) {
                    students[i].email = document.querySelector("#emailNew").value;
                    localStorage.setItem("Students", JSON.stringify(students));
                    document.querySelector("#formStudent").innerHTML = "";
                    document.querySelector("#tableStudents").innerHTML = ""; // очищаем блок, в котором находится таблица, чтобы не было дублирования таблицы
                    add(students);
                    document.querySelector("#tableStatistic").innerHTML = "";
                    addStatistics(students);
                } else if (e.keyCode === 13) {
                    alert("The email address was entered incorrectly");
                }
            });
            document.querySelector("#emailNew").addEventListener("blur", function () {
                document.querySelector("#emailNew").parentNode.innerHTML = students[i].email;
            });
        });

        numLine++;
    }

    if (Object.keys(students).length == 0) {  //тут я проверяю длину массива, если она равна нулю, выполняю действие
        key = 1;
        localStorage.setItem("key", key);
        document.querySelector("#tableStudents").innerHTML = "";
        var pp = document.createElement("p"); //создаю переменную, которой присваиваю тег р
        pp.classList.add("pp");
        pp.innerHTML = "Студенты не найдены"; //записываю в переменную строку
        document.querySelector("#tableStudents").append(pp); //указываю, куда записать переменную в html
    }

    var form = document.createElement("div"); //создаю переменную в которую вкладываю блок, далее в блоке я создаю форму для добавления студента
    form.innerHTML = '<form action="">' +
        '<input type="text" id="name" name="name" value="" placeholder = "Name"> ' +
        '<input type="text" id="estimate" name="estimate" value="" placeholder = "Estimate"> ' +
        '<input type="text" id="course" name="Course" value="" placeholder = "Course"> ' +
        '<input type="text" id="emailAddress" name="emailAddress" value="" placeholder = "Email"> ' +
        '<input type="checkbox" id="activity" name="activity" checked> ' +
        '<label for="activity">Student activity</label> ' +
        '<input type="button" id="submit" value="Добавить">' +
        '</form>';
    document.querySelector("#formStudent").append(form); //определяю куда будет записана форма в html документе

    document.querySelector("#submit").addEventListener("click", function () {  //обращаемся к кнопке через айди и прослушиваем ее на клик
        let checkName = /^[A-ZА-Я][a-zа-я]{1,14}$/.test(document.querySelector("#name").value);
        let checkCoure = /^[1-5]$/.test(document.querySelector("#course").value);
        let checkEmail = /^.{1,25}\@{1}[a-z]{1,9}(\.[a-z]{2,4}){1,2}$/.test(document.querySelector("#emailAddress").value);
        let checkEstimate = document.querySelector("#estimate").value;
        if (checkName && checkCoure && checkEmail && checkEstimate <= 5 && checkEstimate > 0) {
            var stN = {}; //создаю пустой объект, для записи в него описания объекта (имя, курс, оценка, активность)
            stN.name = document.querySelector("#name").value; // записываю в объект ключ name и в него записываю значение из формы по айди
            stN.estimate = Number(document.querySelector("#estimate").value); //тоже самое по аналогии. пишу .value чтобы брать значение, а не ссылку на это значение
            stN.course = document.querySelector("#course").value;
            stN.email = document.querySelector("#emailAddress").value;
            stN.date = {
                allTime: Date.now() / 100 / 60 / 60,
                hours: (new Date).getHours(),
                minutes: (new Date).getMinutes(),
                year: (new Date).getFullYear(),
                month: (new Date).getMonth(),
                day: (new Date).getDate()
            };
            if (document.querySelector("#activity").checked == true) { // для записи активности, нужно определить какое значение тру или фолс у студента, для этого мы используем checked, который проверяет стоит ли галочка в чекбоксе
                stN.active = true; //если галочка стоит, тогда присваиваем тру
            } else {
                stN.active = false; //если не стоит, тогда фолс
            }
            console.log(typeof (key));
            students["stN" + key] = stN;
            key = Number(key) + 1;

            console.log(typeof (key)); //записываем объект нового студента в наш общий объект студентов, для уникальности ключа с каждым шагом меняем переменну j
            localStorage.setItem("key", key);
            localStorage.setItem("Students", JSON.stringify(students));
            document.querySelector("#formStudent").innerHTML = "";
            document.querySelector("#tableStudents").innerHTML = ""; // обнуляем таблицу после добавления студента, иначе она будет дублироваться
            document.querySelector("#tableStatistic").innerHTML = "";
            add(students); //запускаем снова функцию добавления студентов из объекта в стаблицу
            addStatistics(students); //запускаю функцию, которая посчитает ср оценки и неактивных студентов из обновленного объекта студентов
        } else {
            alert("Error");
        }
    });

    let header = document.createElement("p");
    header.innerHTML = "Студенты добавленные за последний час : " + newStudentsNumber;
    document.querySelector("#tableStatistic").append(header);
}

add(students);

function addStatistics() {

    let heading = document.createElement("h2"); //создаю переменную, которой присваиваю тег р
    heading.innerHTML = "Статистика студентов"; //записываю в переменную строку
    document.querySelector("#tableStatistic").append(heading); //указываю, куда записать переменную в html

    var studentsActiveSr = {}; //создаем объект, который выаедет нам ср. оценку в разрезе каждого курса
    var inactiveStudents = {};
    var maxCourse = 1;

    for (let i in students) {

        if (students[i].course > maxCourse) {
            maxCourse = students[i].course;
        }
    }

    for (let i = 1; i <= maxCourse; i++) {
        var sum = 0;
        var sr = 0;
        var kolStud = 0;
        var kolStadInnective = 0;

        for (let j in students) {

            if (students[j].active == true && students[j].course == i) { //проверяет на активность студента и его принадлежность к определенному курсу
                kolStud++; //каждый шаг увеличиваем кол-во студентов на 1
                sum += students[j].estimate; //каждый шаг плюсуем к сумме след-ю оценку студент
            }
            if (students[j].active == false && students[j].course == i) {
                kolStadInnective++;
            }
        }

        if (sum !== 0) {

            sr = parseInt(sum / kolStud * 100) / 100; //считаем ср. оценку студента и округляем до сотых
            studentsActiveSr["Course " + i] = sr; //записываем значение средней оценки в объект 

        }

        if (kolStadInnective !== 0) {
            inactiveStudents["Course " + i] = kolStadInnective;
        }
    }

    var tableStatistic = document.createElement("table"); // создаю переменную и присваиваю ей тег таблицы
    document.querySelector("#tableStatistic").append(tableStatistic); //определяю куда будет записана таблица в html

    var line = document.createElement("tr");
    line.innerHTML = "<td>Курсы";

    var line2 = document.createElement("tr");
    line2.innerHTML = "<td>Средние оценки";

    var line3 = document.createElement("tr");
    line3.innerHTML = "<td>Курсы";

    var line4 = document.createElement("tr");
    line4.innerHTML = "<td>Неактивные студенты";
    tableStatistic.append(line, line2, line3, line4);

    for (let i in studentsActiveSr) {
        let cell = document.createElement("td");
        cell.innerHTML = i;
        line.append(cell);
        cell = document.createElement("td");
        if (studentsActiveSr[i] <= 3) {
            cell.classList.add("losers");
        } else if (studentsActiveSr[i] > 3 && studentsActiveSr[i] <= 4) {
            cell.classList.add("normal");
        } else if (studentsActiveSr[i] > 4) {
            cell.classList.add("successful");
        }
        cell.innerHTML = studentsActiveSr[i];
        line2.append(cell);
    }

    for (let i in inactiveStudents) {
        let cell = document.createElement("td");
        cell.innerHTML = i;
        line3.append(cell);
        cell = document.createElement("td");
        cell.innerHTML = inactiveStudents[i];
        line4.append(cell);
    }

    if (Object.keys(students).length == 0) {  //тут я проверяю длину массива, если она равна нулю, выполняю действие
        document.querySelector("#tableStatistic").innerHTML = ""; //обнуляю таблицу, чтобы пропала первая строка с заголовками
    }

    return {
        studentsActiveSr: studentsActiveSr,
        inactiveStudents: inactiveStudents
    }
}

addStatistics();

/*
Добавить еще одно свойство для студента - email адрес
выполнено строка 3
*/
/*
Вывести mail адрес на ряду в списке студентов
выполнено строка 184, 199
*/
/*
Так же организовать возможность изменения по аналогии с именем
выполнено строка 279
*/
/*
Написать регулярное выражение проверки введенных данных email
выполнено строка 294
*/
/*
Написать валидацию проверку ввода данных - курс это любое целое число от 1 до 5,
email - с помощью регулярки, имя может содержать только буквы не более 15 символов
выполнено строка name 227 estimate 248 course 268 email 293
*/
/*
Если при вводе в любую из форм введены невалидные данные - сообщать об этом в окне с ошибкой
239 259 280 302
*/
/*
Изменять данные физически тогда и только тогда, когда будут введены корректные
233 252 273 295
*/
/*
Добавить еще одно свойство: date - дата создания студента,
брать текущее время клиента и записывать наряду с каждым студентом, выводить дату в списке со студентами -
ее нельзя изменить.
190,458
*/
/*
Добавить в статистику еще одно вычисление:  количество студентов которые были добавлены за последний час.
+
*/
/*
Все данные хранить в localStorage касательно студентов
+
*/
/*
При открытии страницы выбирать из localstorage и показывать список студентов и статистику соответственно
+
*/
/*
Если localstorage не имеет никаких данных в себе, показывать что ничего нет. при добавлении из формы нового студента
- данные должны попасть и сохраниться в localstorage, при следующем открытии страницы - уже подтянуться сохраненные данные
+
*/
