/*
Переменная хранит в себе значение от 0 до 9. Написать скрипт который будет выводить слово “один”,
 если переменная хранит значение 1. 
Выводить слово “два” - если переменная хранит значение 2, и т.д. для всех цифр от 0 до 9. 
Реализовать двумя способами.
*/

var a = 1; 
var res = 0;

switch(a){
    case 1: res = "one"; break;
    case 2: res = "two"; break;
    case 3: res = "three";break;
    case 4: res = "four"; break;
    case 5: res = "five"; break;
    case 6: res = "six"; break;
    case 7: res = "seven"; break;
    case 8: res = "eight"; break;
    case 9: res = "nine"; break;
    default: res = 0;
}
console.log(res);

var c = 9; 
var ress = 0;

switch(c){
    case 1: ress = alert("one"); 
    break;
    case 2: ress = alert("two"); 
    break;
    case 3: ress = alert("three");
    break;
    case 4: ress = alert("four"); 
    break;
    case 5: ress = alert("five"); 
    break;
    case 6: ress = alert("six");
    break;
    case 7: ress = alert("seven"); 
    break;
    case 8: ress = alert("eight"); 
    break;
    case 9: ress = alert("nine"); 
    break;
    default: ress = alert("nothing");
}

var b = 8;   

if (b === 0) {
    alert("zero");
}
else if (b === 1) {
    alert("one");
} 
else if (b === 2) {
    alert("two");
}
else if (b === 3) {
    alert("three");
}
else if (b === 4) {
    alert("four");
}
else if (b === 5) {
    alert("five");
}
else if (b === 6) {
    alert("six");
}
else if (b === 7) {
    alert("seven");
}
else if (b === 8) {
    alert("eight");
}
else if (b === 9) {
    alert("nine");
}
else {
    alert("nothing")
}

/*
Переменная хранит в себе значение, напишите скрипт которое выводит информацию о том,
 что число является нулевым либо положительным либо отрицательным.
*/

var a = 5; 

if (a > 0) {
    alert("Variable positive")
}
else if (a == 0) {
    alert("Variable is zero")
}
else {
    alert("Variable negative")
}

/*
Переменная хранит в себе единицу измерения одно из возможных значений (Byte, KB, MB, GB), 
Вторая переменная хранит в себе целое число. В зависимости от того какая единица измерения написать скрипт, 
который выводит количество байт. 
Для вычисления принимает счет что в каждой последующей единицы измерения хранится 1024 единиц более меньшего измерения.
*/

var a = "gbr";
var b = 1;
var res = 0;
switch(a){
    case "byte": res = b; 
    break; 
    case "kb": res = b*1024; 
    break; 
    case "mb": res = b*1024*1024; 
    break; 
    case "gb": res = b*1024*1024*1024; 
    break; 
    default: res = "incorrect";
}
alert(res);

/*
Переменная хранит процент кредита, вторая переменная хранит объем тела кредита, 
третья переменная хранит длительность кредитного договора в годах. Написать скрипт который вычислит:
Сколько процентов заплатит клиент за все время
Сколько процентов заплатит клиент за один календарный год
Какое общее количество денежных средств клиента банка выплатит за все года
*/

var kreditPercent = 10; 
var kredit = 1000; 
var kreditTime = 5;

var fullTimePercent = (kreditPercent * kreditTime);
var moneyKreditTime = (kredit * kreditPercent / 100 * kreditTime)
var moneyOneYear = (kredit * kreditPercent / 100)
var moneyFullTime = (moneyKreditTime + kredit);
alert("Customer will pay for all the time: " + moneyKreditTime + " uah " + "(" + fullTimePercent  + "%)");
alert("The client will pay for 1 year: " + moneyOneYear + " uah " + "(" + kreditPercent + "%)");
alert("The client will pay to the bank: " + moneyFullTime + " uah");

