"use strict"
/*
Посчитать количество ссылок на странице, вывести их содержимое
*/

var a = document.querySelectorAll("a"); //создаем переменную а, в которую записываем массив всех ссылок страницы с помощью метода querySelectorAll, в "a" мы выбираем эл.(тег) который будет записывать
var numberLinks = 0; // создаем переменную для подсчета кол-ва ссылок 

for (let i = 0; i < a.length; i++) { //бегаем по массиву а в котором все ссылки страницы
    var p = document.createElement("p"); // создаем переменную р, чтобы в нее записать новый тег <p>, который появится в html
    p.innerHTML = a[i].getAttribute("href"); // переменную р записываем в html, которая равна каждому значению из массива а, используя метов получения атрибута из этого значения (из тега <a> мы показываем только то, что в атрибуте href)
    document.querySelector("#links").appendChild(p); //обращаемся к элементу с селектором links и в конец этого элемента записываем переменную р с помощью метода appendChild 
    numberLinks++;
}

var number = document.createElement("p"); // создаю переменную, в которой создам <p>
number.innerHTML = "Кол-во ссылок = " + numberLinks; // запишу эту перменную в html,она будет равна текст + кол-во ссылок
document.querySelector("#links").appendChild(number); // обращаемся к элементу с селектором links, чтобы записать number в конец этого элемента

/*
Посчитать количество тегов “p” на странице которые имеют класс “phrase” - вывести их содержимое
*/

var masP = document.querySelectorAll(".phrase"); // присваиваю переменной все элементы класса “phrase”
var numberParagraph = 0; // создаю переменную для подсчета параграфов 

for (let i = 0; i < masP.length; i++) { //бегаю по массиву параграфов с классом “phrase”
    var par = document.createElement("p"); // создаю переменную для дальнейшего вывода параграфов с классом “phrase” и присваиваю ей тег р
    par.innerHTML = masP[i].innerText; //записываю в переменную par i-e значение из массива параграфов
    document.querySelector("#paragraph").appendChild(par); //обращаюсь к айди #paragraph и записываю туда переменную par
    numberParagraph++; // увеличиваю кол-во параграфов на 1
}

var numberP = document.createElement("p"); // создаю переменную для вывода кол-ва парагрфов и присваиваю ей тег р
numberP.innerHTML = "Кол-во параграфов = " + numberParagraph; // записываю в переменную значения для вывода на странице 
document.querySelector("#paragraph").appendChild(numberP); //определяю куда на странице запишу переменную numberP

/*
В задании из пятого урока, взять массив со студентами и вывести их на страницу согласно сверстанной HTML-структуре,
рядом с каждым студентом вывести крестик - по нажатию на который студент будет удален (удаляется как со страницы, так и с объекта), 
если был удален последний студент написать текстовое сообщение (“Студенты не найдены”)
*/

let students = {
    st1: {
        name: "Vanya",
        estimate: 4.2,
        course: 10,
        active: true,
    },
    st2: {
        name: "Alex",
        estimate: 5,
        course: 11,
        active: false,
    },
    st3: {
        name: "Vova",
        estimate: 2,
        course: 2,
        active: false,
    },
    st4: {
        name: "Victoria",
        estimate: 3.2,
        course: 5,
        active: true,
    },
    st5: {
        name: "Viktor",
        estimate: 4.2,
        course: 15,
        active: true,
    },
    st6: {
        name: "Pavel",
        estimate: 3.3,
        course: 1,
        active: false,
    },
    st7: {
        name: "Bogdan",
        estimate: 4.4,
        course: 100,
        active: true,
    },
    st8: {
        name: "Julia",
        estimate: 3.1,
        course: 3,
        active: false,
    },
    st9: {
        name: "Artem",
        estimate: 4.9,
        course: 2,
        active: true,
    },
    st10: {
        name: "Georgiy",
        estimate: 2.5,
        course: 5,
        active: false,
    },
    st11: {
        name: "Andrey",
        estimate: 4.4,
        course: 100,
        active: true,
    },
    st12: {
        name: "Kate",
        estimate: 3.6,
        course: 3,
        active: false,
    },
    st13: {
        name: "Natali",
        estimate: 4.8,
        course: 1,
        active: true,
    },
    st14: {
        name: "Svetlana",
        estimate: 4.9,
        course: 3,
        active: false,
    },
    st15: {
        name: "Yra",
        estimate: 1.5,
        course: 2,
        active: false,
    },
    st16: {
        name: "Leo",
        estimate: 3,
        course: 4,
        active: true,
    },
    st17: {
        name: "Sergey",
        estimate: 3.9,
        course: 4,
        active: false,
    },
    st18: {
        name: "Maria",
        estimate: 4.3,
        course: 5,
        active: true,
    },
    st19: {
        name: "Alena",
        estimate: 4.4,
        course: 1,
        active: false,
    },
    st20: {
        name: "Olga",
        estimate: 4.5,
        course: 2,
        active: true,
    },
}

var j = 1; // для записи ключей новых студентов из задания 6.5, каждый ключ будет уникальный, но не будет соответствовать порядку первоначальных ключей st1,st2...
function add(students) { //функция добавления студентов в таблицу 

    let zag1 = document.createElement("h2"); //создаю переменную, которой присваиваю тег р
    zag1.innerHTML = "Вывод студентов"; //записываю в переменную строку
    document.querySelector("#tableStudents").append(zag1); //указываю, куда записать переменную в html

    var table = document.createElement("table"); //создаю переменную для записи в нее тега таблицы
    table.innerHTML = "<tr><th>Name<th>Estimate<th>Course<th>Active<th>Deletion"; // добавляю первую строку в таблицу
    document.querySelector("#tableStudents").append(table); //определяю куда буду записывать таблицу в html

    for (let i in students) { //бегаю по объекту со студентами, с каждым новым циклом добавляю новую строку со след-им студентом

        var line = document.createElement("tr"); // присваиваю создание тега строки
        var cell = document.createElement("td"); // присваиваю 
        cell.innerHTML = students[i].name; // записываю внутрь ячейки имя студента

        var cell2 = document.createElement("td"); //создаю ячейки в строку для описания студента
        cell2.innerHTML = students[i].estimate;

        var cell3 = document.createElement("td");
        cell3.innerHTML = students[i].course;

        var cell4 = document.createElement("td");
        cell4.innerHTML = students[i].active;

        var cell5 = document.createElement("td");
        cell5.innerHTML = '<input type = "checkbox" id="deletion" name="deletion">'; // создаю галочку для удаления студента из объекта

        line.append(cell, cell2, cell3, cell4, cell5); // записываю ячейки в строку
        document.querySelector("tbody").append(line); // записываю строку в тела таблицы

        cell5.addEventListener("click", function () { // медот проверяет ячейку в которой расположена галочка на клик, если он был, выполняется функция
            delete students[i]; //удаляется студент, где i это имя ключа 
            document.querySelector("#tableStudents").innerHTML = ""; // очищаем блок, в котором находится таблица, чтобы не было дублирования таблицы
            add(students); //запускаю заново функцию добавления студентов в таблицу
            document.querySelector("#tableStatistic").innerHTML = "";
            addStatistics(students);
        }
        );
    }
    if (Object.keys(students).length == 0) {  //тут я проверяю длину массива, если она равна нулю, выполняю действие
        document.querySelector("#tableStudents").innerHTML = "";
        var pp = document.createElement("p"); //создаю переменную, которой присваиваю тег р
        pp.classList.add("pp");
        pp.innerHTML = "Студенты не найдены"; //записываю в переменную строку
        document.querySelector("#tableStudents").append(pp); //указываю, куда записать переменную в html
    }
}

add(students);

/*
Вывести статистику средних оценок в разрезе курса и статистику по количеству неактивных 
студентов в разрезе каждого курса и общее количество неактивных студентов
*/

function addStatistics() {

    let zag = document.createElement("h2"); //создаю переменную, которой присваиваю тег р
    zag.innerHTML = "Статистика студентов"; //записываю в переменную строку
    document.querySelector("#tableStatistic").append(zag); //указываю, куда записать переменную в html

    var studentsActiveSr = {}; //создаем объект, который выаедет нам ср. оценку в разрезе каждого курса
    var inactiveStudents = {};
    var maxCourse = 1;

    for (let i in students) {
        if (students[i].course > maxCourse) {
            maxCourse = students[i].course;
        }
    }

    for (let i = 1; i <= maxCourse; i++) {
        var sum = 0;
        var sr = 0;
        var kolStud = 0;
        var kolStadInnective = 0;

        for (let j in students) {
           
            if (students[j].active == true && students[j].course == i) { //проверяет на активность студента и его принадлежность к определенному курсу
                kolStud++; //каждый шаг увеличиваем кол-во студентов на 1
                sum += students[j].estimate; //каждый шаг плюсуем к сумме след-ю оценку студент
            }
            if (students[j].active == false && students[j].course == i) {
                kolStadInnective++;
            }
        }
       
        if (sum !== 0) {
            sr = parseInt(sum / kolStud * 100) / 100; //считаем ср. оценку студента и округляем до сотых
            studentsActiveSr["Course " + i] = sr; //записываем значение средней оценки в объект 
           
        }
        if (kolStadInnective !== 0) {
            inactiveStudents["Course " + i] = kolStadInnective;
        }
        if (sum !== 0 && kolStadInnective == 0) {
            inactiveStudents["Course " + i] = 0;
        }
    }

    var tableStatistic = document.createElement("table"); // создаю переменную и присваиваю ей тег таблицы
    document.querySelector("#tableStatistic").append(tableStatistic); //определяю куда будет записана таблица в html

    var line = document.createElement("tr");
    line.innerHTML = "<td>Курсы";

    var line2 = document.createElement("tr");
    line2.innerHTML = "<td>Средние оценки";

    var line3 = document.createElement("tr");
    line3.innerHTML = "<td>Курсы";

    var line4 = document.createElement("tr");
    line4.innerHTML = "<td>Неактивные студенты";
    tableStatistic.append(line, line2, line3, line4);

    for (let i in studentsActiveSr) {
        let cell = document.createElement("td");
        cell.innerHTML = i;
        line.append(cell);
        cell = document.createElement("td");
        cell.innerHTML = studentsActiveSr[i];
        line2.append(cell);
    }

    for (let i in inactiveStudents) {
        let cell = document.createElement("td");
        cell.innerHTML = i;
        line3.append(cell);
        cell = document.createElement("td");
        cell.innerHTML = inactiveStudents[i];
        line4.append(cell);
    }

    if (Object.keys(students).length == 0) {  //тут я проверяю длину массива, если она равна нулю, выполняю действие
        document.querySelector("#tableStatistic").innerHTML = ""; //обнуляю таблицу, чтобы пропала первая строка с заголовками
    }

    return {
        studentsActiveSr: studentsActiveSr,
        inactiveStudents: inactiveStudents
    }
}

addStatistics();

/*
Добавить текстовое поле ввода - ввод имени студента, поле ввода для курса, оценки и checkbox для активности студента,
по нажатии на кнопку “Добавить” - студент новый добавляется в список студентов
*/

var form = document.createElement("div"); //создаю переменную в которую вкладываю блок, далее в блоке я создаю форму для добавления студента
form.innerHTML = '<form action="">' +
    '<input type="text" id="name" name="name" value="Name"> ' +
    '<input type="text" id="estimate" name="estimate" value="Estimate"> ' +
    '<input type="text" id="course" name="Course" value="Course"> ' +
    '<input type="checkbox" id="activity" name="activity" checked> ' +
    '<label for="activity">Student activity</label> ' +
    '<input type="button" id="submit" value="Добавить">' +
    '</form>';
document.querySelector("#formStudent").append(form); //определяю куда будет записана форма в html документе

document.querySelector("#submit").addEventListener("click", function () {  //обращаемся к кнопке через айди и прослушиваем ее на клик

    var stN = {}; //создаю пустой объект, для записи в него описания объекта (имя, курс, оценка, активность)
    stN.name = document.querySelector("#name").value; // записываю в объект ключ name и в него записываю значение из формы по айди
    stN.estimate = Number(document.querySelector("#estimate").value); //тоже самое по аналогии. пишу .value чтобы брать значение, а не ссылку на это значение
    stN.course = document.querySelector("#course").value;

    if (document.querySelector("#activity").checked == true) { // для записи активности, нужно определить какое значение тру или фолс у студента, для этого мы используем checked, который проверяет стоит ли галочка в чекбоксе
        stN.active = true; //если галочка стоит, тогда присваиваем тру
    } else {
        stN.active = false; //если не стоит, тогда фолс
    }

    students["stN" + j++] = stN; //записываем объект нового студента в наш общий объект студентов, для уникальности ключа с каждым шагом меняем переменну j
    console.log(students)
    document.querySelector("#tableStudents").innerHTML = ""; // обнуляем таблицу после добавления студента, иначе она будет дублироваться
    document.querySelector("#tableStatistic").innerHTML = "";
    add(students); //запускаем снова функцию добавления студентов из объекта в стаблицу
    addStatistics(students); //запускаю функцию, которая посчитает ср оценки и неактивных студентов из обновленного объекта студентов
});

