"use strict"

/*
для изменения содержимого 
var pp = document.querySelectorAll("p").length - 1; // 
document.querySelectorAll("p")[pp].innerHTML = "Параграф 2"; //в боди добавляю p
*/

/*
В задании из пятого урока, взять массив со студентами и вывести их на страницу согласно сверстанной HTML-структуре,
 рядом с каждым студентом вывести крестик - по нажатию на который студент будет удален (удаляется как со страницы, так и с объекта), 
 если был удален последний студент написать текстовое сообщение (“Студенты не найдены”)
*/

let students = {
    st1: {
        name: "Vanya",
        estimate: 4.2,
        course: 5,
        active: true,
    },
    st2: {
        name: "Alex",
        estimate: 5,
        course: 4,
        active: false,
    },
    st3: {
        name: "Vova",
        estimate: 2,
        course: 2,
        active: false,
    },
    st4: {
        name: "Victoria",
        estimate: 3.2,
        course: 5,
        active: true,
    },
    st5: {
        name: "Viktor",
        estimate: 4.2,
        course: 2,
        active: true,
    },
    st6: {
        name: "Pavel",
        estimate: 3.3,
        course: 1,
        active: false,
    },
    st7: {
        name: "Bogdan",
        estimate: 4.4,
        course: 1,
        active: false,
    },
    st8: {
        name: "Julia",
        estimate: 3.1,
        course: 3,
        active: false,
    },
    st9: {
        name: "Artem",
        estimate: 4.9,
        course: 2,
        active: true,
    },
    st10: {
        name: "Georgiy",
        estimate: 2.5,
        course: 5,
        active: false,
    },
    st11: {
        name: "Andrey",
        estimate: 3.1,
        course: 5,
        active: true,
    },
    st12: {
        name: "Kate",
        estimate: 3.6,
        course: 3,
        active: false,
    },
    st13: {
        name: "Natali",
        estimate: 4.8,
        course: 1,
        active: true,
    },
    st14: {
        name: "Svetlana",
        estimate: 4.9,
        course: 3,
        active: false,
    },
    st15: {
        name: "Yra",
        estimate: 1.5,
        course: 2,
        active: false,
    },
    st16: {
        name: "Leo",
        estimate: 3,
        course: 4,
        active: true,
    },
    st17: {
        name: "Sergey",
        estimate: 3.9,
        course: 4,
        active: false,
    },
    st18: {
        name: "Maria",
        estimate: 4.3,
        course: 5,
        active: true,
    },
    st19: {
        name: "Alena",
        estimate: 4.4,
        course: 1,
        active: false,
    },
    st20: {
        name: "Olga",
        estimate: 4.5,
        course: 2,
        active: true,
    },
}

//эта часть без изменений из первого варианта дз

var j = 1; // для записи ключей новых студентов из задания 6.5, каждый ключ будет уникальный, но не будет соответствовать порядку первоначальных ключей st1,st2...
function add(students) { //функция добавления студентов в таблицу 

    var table = document.createElement("table"); //создаю переменную для записи в нее тега таблицы
    table.innerHTML = "<tr><th>Name<th>Estimate<th>Course<th>Active<th>Deletion"; // добавляю первую строку в таблицу
    document.querySelector("#tableStudents").append(table); //определяю куда буду записывать таблицу в html

    for (let i in students) { //бегаю по объекту со студентами, с каждым новым циклом добавляю новую строку со след-им студентом

        var line = document.createElement("tr"); // присваиваю создание тега строки
        var cell = document.createElement("td"); // присваиваю 
        cell.innerHTML = students[i].name; // записываю внутрь ячейки имя студента

        var cell2 = document.createElement("td"); //создаю ячейки в строку для описания студента
        cell2.innerHTML = students[i].estimate;

        var cell3 = document.createElement("td");
        cell3.innerHTML = students[i].course;

        var cell4 = document.createElement("td");
        cell4.innerHTML = students[i].active;

        var cell5 = document.createElement("td");
        cell5.innerHTML = '<input type = "checkbox" id="deletion" name="deletion">'; // создаю галочку для удаления студента из объекта

        line.append(cell, cell2, cell3, cell4, cell5); // записываю ячейки в строку
        document.querySelector("tbody").append(line); // записываю строку в тела таблицы

        cell5.addEventListener("click", function () { // медот проверяет ячейку в которой расположена галочка на клик, если он был, выполняется функция
            delete students[i]; //удаляется студент, где i это имя ключа 
            document.querySelector("#tableStudents").innerHTML = ""; // очищаем блок, в котором находится таблица, чтобы не было дублирования таблицы
            add(students); //запускаю заново функцию добавления студентов в таблицу
        }
        );
    }
    if (Object.keys(students).length == 0) {  //тут я проверяю длину массива, если она равна нулю, выполняю действие
        table.innerHTML = ""; //обнуляю таблицу, чтобы пропала первая строка с заголовками
        var p = document.createElement("p"); //создаю переменную, которой присваиваю тег р
        p.innerHTML = "Студенты не найдены"; //записываю в переменную строку
        document.querySelector("#tableStudents").append(p); //указываю, куда записать переменную в html
    }
}

add(students);

/*
Второй вариант добавления ячеек в строку
for (let i in students) {
    var str = document.createElement("tr");
    str.innerHTML = "<td>" + students[i].name;
    str.innerHTML += "<td>" + students[i].estimate;
    document.querySelector("tbody").appendChild(str);
}
*/

/*
Вывести статистику средних оценок в разрезе курса и статистику по количеству неактивных 
студентов в разрезе каждого курса и общее количество неактивных студентов
*/

var student = (function () { //главная самовызывающаяся фун-ция, которая передает вызов внутренних функций

    var studentsActiveAvg = {}; //создаем объект, который выаедет нам ср. оценку в разрезе каждого курса
    function avgEstimate() { //эта функция считает среднюю оценку студентов в разрезе каждого курса
        var sum1 = 0; //переменная считает сумму всех оценок студентов в разрезе указанного курса
        var sum2 = 0;
        var sum3 = 0;
        var sum4 = 0;
        var sum5 = 0;
        var avg1 = 0; //средняя оценка студентов 1-го курса и т.д. 
        var avg2 = 0;
        var avg3 = 0;
        var avg4 = 0;
        var avg5 = 0;
        var kolstud1 = 0; //считает кол-во студентов в разрезе указанного курса
        var kolstud2 = 0;
        var kolstud3 = 0;
        var kolstud4 = 0;
        var kolstud5 = 0;

        for (let i in students) { //бегаем по объекту со студентами

            if (students[i].active == true) { //если студент активен, тогда выполняем след. действия

                switch (students[i].course) { //исходя из значения курса, считаю сумму оценок по каждому курсу

                    case 1: sum1 += students[i].estimate; kolstud1++; break; // для 1 го
                    case 2: sum2 += students[i].estimate; kolstud2++; break; //2 го и тд
                    case 3: sum3 += students[i].estimate; kolstud3++; break;
                    case 4: sum4 += students[i].estimate; kolstud4++; break;
                    case 5: sum5 += students[i].estimate; kolstud5++; break;
                    default: console.log("Incorrect course number"); // на случай, если попалось некорректное значение курса
                }
            }
        }

        avg1 = parseInt(sum1 / kolstud1 * 100) / 100; //считаю средние оценки для каждого курса
        avg2 = parseInt(sum2 / kolstud2 * 100) / 100;
        avg3 = parseInt(sum3 / kolstud3 * 100) / 100;
        avg4 = parseInt(sum4 / kolstud4 * 100) / 100;
        avg5 = parseInt(sum5 / kolstud5 * 100) / 100;

        studentsActiveAvg["Course " + 1] = avg1; //записываю средние оценки в объект 
        studentsActiveAvg["Course " + 2] = avg2;
        studentsActiveAvg["Course " + 3] = avg3;
        studentsActiveAvg["Course " + 4] = avg4;
        studentsActiveAvg["Course " + 5] = avg5;

        return studentsActiveAvg; //возвращаю объект со средними оценками
    }

    var inactiveStudents = {}; //создаю объект с кол-вом неактивных студентов
    var kolStudCourse = 0;
    var kolStud1 = 0;
    var kolStud2 = 0;
    var kolStud3 = 0;
    var kolStud4 = 0;
    var kolStud5 = 0;

    function inactive() { //функция подсчета и записи неактивных студентов

        for (let i in students) { //бегаю по объекту студентов

            if (students[i].active == false) { //если студент неактивен выполняю действия ниже

                switch (students[i].course) { //исходя из значения курса, считаю кол-во неакт студентов в каждом курсе и общее их кол-во

                    case 1: kolStudCourse++; kolStud1++; break; //1 курс и тд
                    case 2: kolStudCourse++; kolStud2++; break;
                    case 3: kolStudCourse++; kolStud3++; break;
                    case 4: kolStudCourse++; kolStud4++; break;
                    case 5: kolStudCourse++; kolStud5++; break;
                    default: console.log("Incorrect course number"); //в случае некорректного номера курса выводим строку
                }
            }
        }

        inactiveStudents["Course " + 1] = kolStud1; //записываю кол-во неактивных студентов в объект
        inactiveStudents["Course " + 2] = kolStud2;
        inactiveStudents["Course " + 3] = kolStud3;
        inactiveStudents["Course " + 4] = kolStud4;
        inactiveStudents["Course " + 5] = kolStud5;
        inactiveStudents["Total inactive students"] = kolStudCourse;

        return inactiveStudents; //возвращаю объект неактивных студентов
    }

    function addStatistics() { //этой функцией я записываю данные предыдущих функций в html

        var tableStatistic = document.createElement("table"); // создаю переменную и присваиваю ей тег таблицы
        tableStatistic.innerHTML = "<tr><th><th>Course 1<th>Course 2<th>Course 3<th>Course 4<th>Course 5<th>Total"; // записываю первую строку с заголовками в таблицу 
        document.querySelector("#tableStatistic").append(tableStatistic); //определяю куда будет записана таблица в html

        let line = document.createElement("tr"); //в рамкам второй таблицы создаю строку
        let cell = document.createElement("td"); //в строку вкладываю ячейки 
        cell.innerHTML = "Средняя оценка";

        for (let i in studentsActiveAvg) { //бегаю по объекту средних оценок студентов

            switch (i) { //исходя из значения курса, выбираю нужный кейс для записи значения в ячейку

                case ("Course 1"):
                    var cell1 = document.createElement("td"); //для певого курса и тд
                    cell1.innerHTML = studentsActiveAvg[i];
                    break;
                case ("Course 2"):
                    var cell2 = document.createElement("td");
                    cell2.innerHTML = studentsActiveAvg[i];
                    break;
                case ("Course 3"):
                    var cell3 = document.createElement("td");
                    cell3.innerHTML = studentsActiveAvg[i];
                    break;
                case ("Course 4"):
                    var cell4 = document.createElement("td");
                    cell4.innerHTML = studentsActiveAvg[i];
                    break;
                case ("Course 5"):
                    var cell5 = document.createElement("td");
                    cell5.innerHTML = studentsActiveAvg[i];
                    break;
                default: console.log("Error");
            }
        }

        let cell6 = document.createElement("td"); // добавляю пустую ячейку в таблице для симетричности 
        cell6.innerHTML = ""; // записываю в нее ничего

        let line2 = document.createElement("tr"); //создаю следующую строку
        let cell7 = document.createElement("td"); //вкладываю в нее ячейки по такому же принципу
        cell7.innerHTML = "Неактивные студенты";

        for (let j in inactiveStudents) { //бегаю по объекту с кол-вом неактивных студентов

            switch (j) {

                case ("Course 1"):
                    var cell8 = document.createElement("td");
                    cell8.innerHTML = inactiveStudents[j];
                    break;
                case ("Course 2"):
                    var cell9 = document.createElement("td");
                    cell9.innerHTML = inactiveStudents[j];
                    break;
                case ("Course 3"):
                    var cell10 = document.createElement("td");
                    cell10.innerHTML = inactiveStudents[j];
                    break;
                case ("Course 4"):
                    var cell11 = document.createElement("td");
                    cell11.innerHTML = inactiveStudents[j];
                    break;
                case ("Course 5"):
                    var cell12 = document.createElement("td");
                    cell12.innerHTML = inactiveStudents[j];
                    break;
                case ("Total inactive students"):
                    var cell13 = document.createElement("td");
                    cell13.innerHTML = inactiveStudents[j];
                    break;
                default: console.log("Error");
            }
        }

        document.querySelectorAll("tbody")[1].append(line); //обращаюсь к телу второй таблицы на странице и записываю туда первую строку
        line.append(cell, cell1, cell2, cell3, cell4, cell5, cell6); //в строку записываю все заполенные ячейки

        line2.append(cell7, cell8, cell9, cell10, cell11, cell12, cell13); //тоже самое со второй строкой
        document.querySelectorAll("tbody")[1].append(line2);
    }

    return { //главная функция возвращает значения всез вложеных в ней функций

        avgEstimate: avgEstimate,
        inactive: inactive,
        addStatistics: addStatistics,
    }
})();

student.avgEstimate();
student.inactive();
student.addStatistics();

/*
Добавить текстовое поле ввода - ввод имени студента, поле ввода для курса, оценки и checkbox для активности студента,
 по нажатии на кнопку “Добавить” - студент новый добавляется в список студентов
*/

//без изменений из первого варианта 

var form = document.createElement("div"); //создаю переменную в которую вкладываю блок, далее в блоке я создаю форму для добавления студента
form.innerHTML = '<form action="">' +
    '<input type="text" id="name" name="name" value="Name"> ' +
    '<input type="text" id="estimate" name="estimate" value="Estimate"> ' +
    '<input type="text" id="course" name="Course" value="Course"> ' +
    '<input type="checkbox" id="activity" name="activity" checked> ' +
    '<label for="activity">Student activity</label> ' +
    '<input type="button" id="submit" value="Добавить">' +
    '</form>';
document.querySelector("#formStudent").append(form); //определяю куда будет записана форма в html документе

document.querySelector("#submit").addEventListener("click", function () {  //обращаемся к кнопке через айди и прослушиваем ее на клик

    var stN = {}; //создаю пустой объект, для записи в него описания объекта (имя, курс, оценка, активность)
    stN.name = document.querySelector("#name").value; // записываю в объект ключ name и в него записываю значение из формы по айди
    stN.estimate = document.querySelector("#estimate").value; //тоже самое по аналогии. пишу .value чтобы брать значение, а не ссылку на это значение
    stN.course = document.querySelector("#course").value;

    if (document.querySelector("#activity").checked == true) { // для записи активности, нужно определить какое значение тру или фолс у студента, для этого мы используем checked, который проверяет стоит ли галочка в чекбоксе
        stN.active = true; //если галочка стоит, тогда присваиваем тру
    } else {
        stN.active = false; //если не стоит, тогда фолс
    }

    students["stN" + j++] = stN; //записываем объект нового студента в наш общий объект студентов, для уникальности ключа с каждым шагом меняем переменну j
    document.querySelector("#tableStudents").innerHTML = ""; // обнуляем таблицу после добавления студента, иначе она будет дублироваться
    add(students); //запускаем снова функцию добавления студентов из объекта в стаблицу
});

