"use strict"
let ingradients = {
    1: {
        id: 1,
        title: "ingr1",
        protein: 27.5,
        carbohydrates: 12.3,
        fats: 4.0,
        kilocalories: 112,
    },
    2: {
        id: 2,
        title: "ingr2",
        protein: 40.5,
        carbohydrates: 10.3,
        fats: 7.0,
        kilocalories: 112,
    },
    3: {
        id: 3,
        title: "ingr3",
        protein: 107.5,
        carbohydrates: 8.3,
        fats: 30.0,
        kilocalories: 112,
    },
    4: {
        id: 3,
        title: "ingr3",
        protein: 77.5,
        carbohydrates: 0.3,
        fats: 13.0,
        kilocalories: 112,
    },
    5: {
        id: 5,
        title: "ingr5",
        protein: 77.5,
        carbohydrates: 0.3,
        fats: 13.0,
        kilocalories: 112,
    },

};

let dishes = [
    {
        id: 1,
        title: "Название блюда 1",
        gram: 350,
        ingredients: [
            {
                id: 1,
                gram: 12,
            }, {
                id: 2,
                gram: 75,
            }, {
                id: 3,
                gram: 150,
            },
        ]
    }, {
        id: 3,
        title: "Название блюда 3",
        gram: 350,
        ingredients: [
            {
                id: 1,
                gram: 12,
            },
        ]
    },
    {
        id: 2,
        title: "Название блюда 2",
        gram: 350,
        ingredients: [
            {
                id: 1,
                gram: 25,
            }, {
                id: 2,
                gram: 23,
            }, {
                id: 4,
                gram: 50,
            },
        ]
    },
];
console.log(ingradients);
console.log(dishes);

// var a = {} создаем пустой объект, в объекте нужно задавать ключи, в отличии от массива
//var b = [{},[{},{}],{}] в массиве можем создавать еще обекты 

let counter = {};

for (var i in ingradients) {
    counter[i] = 0;
}

let index = 0;
let min = false;

for (let i = 0; i < dishes.length; i++) {
    let allProtein = 0;
    let allCarbohydronates = 0;
    let allFats = 0;
    let allKkal = 0;

    for (let j = 0; j < dishes[i].ingredients.length; j++) {
        let id = dishes[i].ingredients[j].id;
        let gram = dishes[i].ingredients[j].gram;
        let protein = ingradients[id].protein;
        let carbohydrate = ingradients[id].carbohydrates;
        let fats = ingradients[id].fats;
        let kilocalories = ingradients[id].kilocalories;

        counter[id]++;

        allProtein += (gram / 100) * protein;
        allCarbohydronates += (gram / 100) * carbohydrate;
        allFats += (gram / 100) * fats;
        allKkal += (gram / 100) * kilocalories;
    }

    let masKolIngr = { 1: 0, 2: 0, 3: 0, 4: 0 };

    for (let i = 0; i < dishes.length; i++) {
        for (let j = 0; j < dishes[i].ingredients.length; j++) {
            let id = dishes[i].ingredients[j].id;
            masKolIngr[id]++;
        }
    }
    console.log(masKolIngr);

    if (min === false) {
        min = allKkal;
    }

    if (min > allKkal) {
        index = i;
        min = allKkal;
    }
  
    console.log(dishes[i].title + " - " + allProtein + " - " + allCarbohydronates + " - " + allFats + " - " + allKkal);
}


console.log(counter);
console.log(dishes[index]);
