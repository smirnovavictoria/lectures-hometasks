/*
Задание 1
Все скрипты которые писали в рамках первого и второго задания - оформить в виде функций
*/
/*
Переменная хранит в себе значение от 0 до 9. Написать скрипт который будет выводить слово “один”,
 если переменная хранит значение 1. 
Выводить слово “два” - если переменная хранит значение 2, и т.д. для всех цифр от 0 до 9. 
Реализовать двумя способами.
*/

function num(a){
    var res = 0;
    switch(a){
        case 1: res = "one"; break;
        case 2: res = "two"; break;
        case 3: res = "three";break;
        case 4: res = "four"; break;
        case 5: res = "five"; break;
        case 6: res = "six"; break;
        case 7: res = "seven"; break;
        case 8: res = "eight"; break;
        case 9: res = "nine"; break;
        default: res = 0;
    }
    return res;
}

console.log(num(8));

function num1(b){
    var ress = 0;
    if (b === 0) {
        ress = "zero";
        alert("zero");
    }
    else if (b === 1) {
        ress = "one";
        alert("one");
    } 
    else if (b === 2) {
        ress = "two";
        alert("two");
    }
    else if (b === 3) {
        ress = "three";
        alert("three");
    }
    else if (b === 4) {
        ress = "four";
        alert("four");
    }
    else if (b === 5) {
        ress = "five";
        alert("five");
    }
    else if (b === 6) {
        ress = "six";
        alert("six");
    }
    else if (b === 7) {
        ress = "seven";
        alert("seven");
    }
    else if (b === 8) {
        ress = "eight";
        alert("eight");
    }
    else if (b === 9) {
        ress = "nine";
        alert("nine");
    }
    else {
        ress = "nothing";
        alert("nothing")
    }
   return ress;
}
console.log(num1(1));

/*
Переменная хранит в себе значение, напишите скрипт которое выводит информацию о том,
 что число является нулевым либо положительным либо отрицательным.
*/

function znak(c){
    if (c > 0) {
        res3 = "Variable positive";
        alert("Variable positive");
    }
    else if (c == 0) {
        res3 = "Variable is zero";
        alert("Variable is zero");
    }
    else {
        res3 = "Variable negative";
        alert("Variable negative");
    }
    return res3;
}

console.log(znak(-1));

/*
Переменная хранит в себе единицу измерения одно из возможных значений (Byte, KB, MB, GB), 
Вторая переменная хранит в себе целое число. В зависимости от того какая единица измерения написать скрипт, 
который выводит количество байт. 
Для вычисления принимает счет что в каждой последующей единицы измерения хранится 1024 единиц более меньшего измерения.
*/

function izmer(a,b){
    switch(a){
        case "byte": res4 = b; 
        break; 
        case "kb": res4 = b*1024; 
        break; 
        case "mb": res4 = b*1024*1024; 
        break; 
        case "gb": res4 = b*1024*1024*1024; 
        break; 
        default: res4 = "incorrect";
        alert(res4);
    }
    return res4;
}
console.log(izmer("gbr",5));

/*
Переменная хранит процент кредита, вторая переменная хранит объем тела кредита, 
третья переменная хранит длительность кредитного договора в годах. Написать скрипт который вычислит:
Сколько процентов заплатит клиент за все время
Сколько процентов заплатит клиент за один календарный год
Какое общее количество денежных средств клиента банка выплатит за все года
*/

function kredit(kreditPercent,kredit,kreditTime,analiz){
    var fullTimePercent = (kreditPercent * kreditTime);
    var moneyKreditTime = (kredit * kreditPercent / 100 * kreditTime)
    var moneyOneYear = (kredit * kreditPercent / 100)
    var moneyFullTime = (moneyKreditTime + kredit);
    alert("Customer will pay for all the time: " + moneyKreditTime + " uah " + "(" + fullTimePercent  + "%)");
    alert("The client will pay for 1 year: " + moneyOneYear + " uah " + "(" + kreditPercent + "%)");
    alert("The client will pay to the bank: " + moneyFullTime + " uah");
    if (analiz == "moneyKreditTime"){
        return moneyKreditTime; 
    }else if (analiz == "moneyOneYear"){
        return moneyOneYear;
    }else {
        return moneyFullTime;
    }
}
console.log(kredit(10, 1000, 5, "moneyFullTime"));

/*
Переменная содержит в себе строку. Вывести строку в обратном порядке.
*/

function someText(text){
    var string = "";
    for(var i = text.length - 1; i > -1; i--){
        string += text[i];
      }
      return string;
}

console.log(someText("JavaScript"));

/*
Переменная содержит в себе число. Написать скрипт который посчитает факториал этого числа.
*/

function f(num){
    var factorial = 1;
    for(var i = 1; i <= num; i++){
        factorial *= i;
    }
    return factorial;
}
console.log(f(5)); 

/*
Дано число - вывести первые N делителей этого числа нацело.
*/

function del(dividend,n){
    var m = [];
    var i = 0;
    for(var divider = 1; i < n; divider++){
        if((dividend % divider) === 0){
            i++;
            m.push(divider);
            console.log("Number of dividers: " + i + " = " + divider);
    }
    else if (dividend < divider){
        console.log("The number of divisors is less than specified")
        break; 
    }
}
 return m;
}
del(20,10); //если нет нужного кол-ва делителей, выведет сколько есть 

/* 
Найти сумму цифр числа которые кратны двум
*/

function multiplicity(numStr, divisor){
    var sum = 0;
    for (i = 0; i < numStr.length; i++){
        if(numStr[i] % divisor === 0){
            var num = Number(numStr[i]);
            sum += num;
        } else if(divisor > 9){
            console.log("The divisor is greater than the allowed value");
            break;
        }
    }
    console.log(sum);
    return sum;   
}

/*
Найти минимальное число которое больше 300 и нацело делиться на 17
*/

function minNum(num,divisor){
    for(var i = num + 1; i > num; i++){
        if(i % divisor === 0){
            console.log(i)
            break;
        }
        else if(num != 300){
            console.log("Mistake! The dividend must be equal to 300 ...");
            break;
        }
        else if(divisor != 17){
            console.log("Mistake! The divisor must be 17 ...");
            break;
        }
    }
    return i;
}

minNum(300,17);

/*
Заданы две переменные для двух целых чисел, найти максимальное общее значение которое нацело делит два заданных числа.
*/

function f2(a, b){
    var maxdev = 0;
    for(var i = 0; i <= a && i <= b; i++){
        if(a % i === 0 && b % i === 0){
            maxdev = i; 
        }
    }
    console.log(maxdev);
    return maxdev;
}

f2(30, 300);
