/*
Задание 1
Все скрипты которые писали в рамках первого и второго задания - оформить в виде функций
*/
/*
Переменная хранит в себе значение от 0 до 9. Написать скрипт который будет выводить слово “один”,
 если переменная хранит значение 1. 
Выводить слово “два” - если переменная хранит значение 2, и т.д. для всех цифр от 0 до 9. 
Реализовать двумя способами.
*/

function num(a){
    var res = 0;
    switch(a){
        case 1: res = "one"; break;
        case 2: res = "two"; break;
        case 3: res = "three";break;
        case 4: res = "four"; break;
        case 5: res = "five"; break;
        case 6: res = "six"; break;
        case 7: res = "seven"; break;
        case 8: res = "eight"; break;
        case 9: res = "nine"; break;
        default: res = 0;
    }
    return res;
}

console.log(num(8));

function num1(b){
    var ress = 0;
    if (b === 0) {
        ress = "zero";
        alert("zero");
    }
    else if (b === 1) {
        ress = "one";
        alert("one");
    } 
    else if (b === 2) {
        ress = "two";
        alert("two");
    }
    else if (b === 3) {
        ress = "three";
        alert("three");
    }
    else if (b === 4) {
        ress = "four";
        alert("four");
    }
    else if (b === 5) {
        ress = "five";
        alert("five");
    }
    else if (b === 6) {
        ress = "six";
        alert("six");
    }
    else if (b === 7) {
        ress = "seven";
        alert("seven");
    }
    else if (b === 8) {
        ress = "eight";
        alert("eight");
    }
    else if (b === 9) {
        ress = "nine";
        alert("nine");
    }
    else {
        ress = "nothing";
        alert("nothing")
    }
   return ress;
}
console.log(num1(1));

/*
Переменная хранит в себе значение, напишите скрипт которое выводит информацию о том,
 что число является нулевым либо положительным либо отрицательным.
*/

function znak(c){
    if (c > 0) {
        res3 = "Variable positive";
        alert("Variable positive");
    }
    else if (c == 0) {
        res3 = "Variable is zero";
        alert("Variable is zero");
    }
    else {
        res3 = "Variable negative";
        alert("Variable negative");
    }
    return res3;
}

console.log(znak(-1));

/*
Переменная хранит в себе единицу измерения одно из возможных значений (Byte, KB, MB, GB), 
Вторая переменная хранит в себе целое число. В зависимости от того какая единица измерения написать скрипт, 
который выводит количество байт. 
Для вычисления принимает счет что в каждой последующей единицы измерения хранится 1024 единиц более меньшего измерения.
*/

function izmer(a,b){
    switch(a){
        case "byte": res4 = b; 
        break; 
        case "kb": res4 = b*1024; 
        break; 
        case "mb": res4 = b*1024*1024; 
        break; 
        case "gb": res4 = b*1024*1024*1024; 
        break; 
        default: res4 = "incorrect";
        alert(res4);
    }
    return res4;
}
console.log(izmer("gbr",5));

/*
Переменная хранит процент кредита, вторая переменная хранит объем тела кредита, 
третья переменная хранит длительность кредитного договора в годах. Написать скрипт который вычислит:
Сколько процентов заплатит клиент за все время
Сколько процентов заплатит клиент за один календарный год
Какое общее количество денежных средств клиента банка выплатит за все года
*/

function kredit(kreditPercent,kredit,kreditTime,analiz){
    var fullTimePercent = (kreditPercent * kreditTime);
    var moneyKreditTime = (kredit * kreditPercent / 100 * kreditTime)
    var moneyOneYear = (kredit * kreditPercent / 100)
    var moneyFullTime = (moneyKreditTime + kredit);
    alert("Customer will pay for all the time: " + moneyKreditTime + " uah " + "(" + fullTimePercent  + "%)");
    alert("The client will pay for 1 year: " + moneyOneYear + " uah " + "(" + kreditPercent + "%)");
    alert("The client will pay to the bank: " + moneyFullTime + " uah");
    if (analiz == "moneyKreditTime"){
        return moneyKreditTime; 
    }else if (analiz == "moneyOneYear"){
        return moneyOneYear;
    }else {
        return moneyFullTime;
    }
}
console.log(kredit(10, 1000, 5, "moneyFullTime"));

/*
Переменная содержит в себе строку. Вывести строку в обратном порядке.
*/

function someText(text){
    var string = "";
    for(var i = text.length - 1; i > -1; i--){
        string += text[i];
      }
      return string;
}

console.log(someText("JavaScript"));

/*
Переменная содержит в себе число. Написать скрипт который посчитает факториал этого числа.
*/

function f(num){
    var factorial = 1;
    for(var i = 1; i <= num; i++){
        factorial *= i;
    }
    return factorial;
}
console.log(f(5)); 

/*
Дано число - вывести первые N делителей этого числа нацело.
*/

function del(dividend,n){
    var m = [];
    var divider = 0;
    for(var i = 1; divider < n; i++){
        if((dividend % i) === 0){
            divider++;
            m.push(i);
            console.log(divider + "  " + i);
    }
    else if (dividend < i){
        console.log("The number of divisors is less than specified")
        break; 
    }
}
 return m;
}
console.log(del(20,10)); //если кол-во делителей нет нужного кол-ва, выведет сколько есть 


/*
Найти сумму цифр числа которые кратны двум
*/

function sum(num){
    var res = 0;
    var numStr = num + "";
    for (var i = 0; i < numStr.length; i++) {
      if (numStr[i] % 2 == 0) {
        var transform = parseInt(numStr[i]);
        res += transform;
      }
    }
    return res;
  }
  console.log(sum(1234567896));


/*
 Найти минимальное число которое больше 300 и нацело делиться на 17
 */

function mindel(min, divisor) {
    var step = 0;
    for (var i = min; step < 1; i++) {
      if (i % divisor == 0) {
        step++;
        return i;
      }
      else if (i < divisor) {
        return console.log("Mistake! The dividend is less than the divisor ...");
        break;
      }
    }
  
  }
  console.log(mindel(300, 17));


/*
Заданы две переменные для двух целых чисел,
найти максимальное общее значение которое нацело делит два заданных числа.
*/

function divider(num1, num2) {
    var minNumber = num1; 
    if (num1 > num2) {
        minNumber = num2; 
    }
    for (var i = minNumber; i > 0; i--) { 
      if (num1 % i == 0 && num2 % i == 0) {
        break;
      }
    }
    return i;
  }
  console.log(divider(100, 200));


/*
Задание 2
 Все скрипты которые используют в своей основе цикл - написать с помощью рекурсивных функций
*/
// Переменная содержит в себе строку. Вывести строку в обратном порядке.

function rev(text, revText, i) {
    if (revText === undefined) {
        revText = "";
      i = text.length - 1;
    }
    if (i > -1) {
        revText += text[i--];
      return rev(text, revText, i);
    }
    else {
      return revText;
    }
  }
  console.log(rev("JavaScript"));

  /*
  Переменная содержит в себе число. Написать скрипт который посчитает факториал этого числа.
  */

 function factorial(num, res, i) {
    if (res === undefined) {
        res = 1;
      i = 1;
    }
    if (i <= num) {
        res *= i++;
      return factorial(num, res, i);
    }
    else {
      return (res);
    }
  }
  console.log(factorial(9));

  /*
  Дано число - вывести первые N делителей этого числа нацело.
  */

 function nDel(num, numDiv, i, n, mDel) {
    if (n === undefined) {
      n = 0;
      i = 1;
      var mDel = [];
    }
    if (num % i == 0 && n < numDiv) {
      n++;
      mDel.push(i);
      i++;
      return nDel(num, numDiv, i, n, mDel);
    }
    else if (num % i != 0 && i < num / 2) {
      i++;
      return nDel(num, numDiv, i, n, mDel);
    }
    else {
      if (n < numDiv) {
        console.log("Mistake! There are fewer divisors than specified")
      }
      return mDel;
    }
  
  }
  console.log(nDel(50, 5));

  /*
   Найти сумму цифр числа которые кратны двум
   */

function summa(num, i, res, numStr) {
  if (res == undefined) {
    res = 0;
    numStr = num + "";
    i = 0;
  }
  if (numStr[i] % 2 === 0 && numStr.length > i) {
    res += parseInt(numStr[i]);
    i++;
    return (summa(num, i, res, numStr));
  } else if (numStr[i] % 2 !== 0 && numStr.length > i) {
    i++;
    return (summa(num, i, res, numStr));
  } else {
    return res;
  }
}
console.log(summa(682179524));

/*
Найти минимальное число которое больше 300 и нацело делиться на 17
*/

function delCel (startNum, del){
    if (startNum%del === 0) {
      var res = startNum;
      return (res);
    }
    else if (startNum%del !== 0) {
        startNum++;
      return delCel (startNum, del);
    }
}

console.log(delCel(300,17));

/*
Заданы две переменные для двух целых чисел, найти максимальное общее значение которое нацело делит два заданных числа.
*/

function deldel (fNum,sNum,del) {
    if (del === undefined) {
      if (fNum>sNum) {
        del = sNum;
      }
      else {
        del = fNum;
      }
    }
    if (fNum%del == 0 && sNum%del ==0) {
      return del;
    }
    else {
        del--;
      return deldel (fNum,sNum,del);
    }
  }
  console.log(deldel(2000,500));

  /*
  Задание 3 
  Написать функцию, которая транспонирует матрицу
  */

 var mas = [];
 function genmas(gor, ver, maxNam) {
   for (var i = 0; i < ver; i++) {
     mas[i] = [];
     for (var j = 0; j < gor; j++) {
       mas[i][j] = Math.floor(Math.random() * maxNam + 1);
     }
   }
   console.log("Array " + gor + "x" + ver + ". Filled with random numbers from 0 to " + maxNam);
   console.log (mas);
   }
 
 function reverse(gor, ver,maxNam) {
    genmas(gor, ver, maxNam);
   var reverseM = [];
   for (var i = 0; i < mas.length; i++) {
     for (var j = 0; j < mas[i].length; j++) {
       if (i == 0) {                          
        reverseM[j] = [];                  
       }
       reverseM[j][i] = mas[i][j];          
     }
   }
   console.log("Inverted array");
   return reverseM;
 }
 console.log (reverse(5,7,13));

 /*
 Задание 4
 Написать функцию, которая складывает две матрицы
 */

function array(gor, ver,maxNum) {
    var mas = [];
    for (var i = 0; i < ver; i++) {
      mas[i] = [];
      for (var j = 0; j < gor; j++) {
        mas[i][j] = Math.floor(Math.random() * maxNum + 1);
      }
    }
    console.log("Array " + gor + "x" + ver + ".Filled with random numbers from 0 to" + maxNum);
    console.log (mas);
    return mas;
    }
  
  function sumMat(gor, ver, maxNum) {
    var summa = [];                                                   
    var matrix1 = array(gor, ver, maxNum);
    var matrix2 = array(gor, ver, maxNum);
    for (var i = 0; i < ver; i++) {
      summa[i] = [];
      for (var j = 0; j < gor; j++) {
        summa[i][j] = matrix1[i][j] + matrix2[i][j];
      }
    }
    console.log ("Sum of matrices");
    return summa;
  }
  console.log(sumMat(2, 4, 11)); 

  /*
  Задание 5
   Найти номер столбца двумерного массива сумма которого является максимальной (аналогично для строки)
   */

  function ganerateMas(gorizontal, vertikal, maxRandomNumber) {
    var mas = [];
    for (var i = 0; i < vertikal; i++) {
      mas[i] = [];
      for (var j = 0; j < gorizontal; j++) {
        mas[i][j] = Math.floor(Math.random() * maxRandomNumber + 1);
      }
    }
    console.log("Array " + gorizontal + "x" + vertikal + ". Filled with random numbers from 0 to" + maxRandomNumber);
    console.log(mas);
    return mas;
  }
  
  
  function maxStolbec(gorizontal, vertikal, maxRandomNumber) {
    var mas = ganerateMas(gorizontal, vertikal, maxRandomNumber);
    var summaStolbMax = 0;
    var numberStolbec = 0;
    for (var i = 0; i < gorizontal; i++) {
      var summaStolb = 0;
      for (var j = 0; j < vertikal; j++) {
        summaStolb += mas[j][i];
      }
      if (summaStolbMax < summaStolb) {
        summaStolbMax = summaStolb;
        numberStolbec = i;
      }
    }
    console.log("Column № " + (numberStolbec) + " has a maximum amount equal to " + summaStolbMax);
  
    return (numberStolbec);
  }
  console.log(maxStolbec(4, 4, 3));
  
  
  function maxStroka(gorizontal, vertikal, maxRandomNumber) {
    var mas = ganerateMas(gorizontal, vertikal, maxRandomNumber);
    var summaStringMax = 0;
    var numberString = 0;
    for (var i = 0; i < vertikal; i++) {
      var summaString = 0;
      for (var j = 0; j < gorizontal; j++) {
        summaString += mas[i][j];
      }
      if (summaStringMax < summaString) {
        summaStringMax = summaString;
        numberString = i;
      }
    }
    console.log("Line № " + (numberString) + " has a maximum amount equal to " + summaStringMax);
    return (numberString);
  }
  console.log(maxStroka(4, 3, 6));
  
  /*
  Задание 6
  Удалить из массива все столбцы которые не имеют ни одного нулевого элемента и сумма которых положительна - оформить в виде функции
*/

function massiv(gorizontal, vertikal, minRandomNumber, maxRandomNumber) {
    var mas = [];
    for (var i = 0; i < vertikal; i++) {
      mas[i] = [];
      for (var j = 0; j < gorizontal; j++) {
        mas[i][j] = Math.floor(Math.random() * (maxRandomNumber + 1 - minRandomNumber) + minRandomNumber);
      }
    }
    return mas;
  }
  
  function remove(gorizontal, vertikal, minRandomNumber, maxRandomNumber) {
    var resultMas = massiv(gorizontal, vertikal, minRandomNumber, maxRandomNumber);
    for (var i = gorizontal - 1; i > -1; i--) {
      var summaStolb = 0;
      var kolNullElementov = 0;
      for (var j = 0; j < vertikal; j++) {
        summaStolb += resultMas[j][i];
        if (resultMas[j][i] == 0) { 
          kolNullElementov++;
        }
      }
      if (kolNullElementov == 0 && summaStolb > 0) {
        for (var t = 0; t < vertikal; t++) {
          resultMas[t].splice(i, 1);
        }
      }
    }
    return (resultMas);
  }
  console.log("All that remains of a randomly generated array");
  console.log(remove(12, 7, -6, 6));
  
