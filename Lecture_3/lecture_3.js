var a = 5;
var b = 2;
var c = 0;

++c;
c += ++c - c-- - --c - --c + ++a + a++ + a + ++b + b - c - --a;
console.log("c = " + c);
/*
a = 7
b = 3
c = -1
0+1+6+6+7+3+3+1-6
0-0+1+6+6+7+3+3+1-6 = 21+c = 22
*/
/*
var d1 = 1;
var d2 = 2;

d1 += d2*2; //d1 = d1 + (d2*2)
*/
var a = [[[10]]];
var b = a[0];

console.log(b[0][0]);

/*
 Получим вывод 3, потому что длина массива 3, а индексация в массиве начинается с 0,
тогда 3 - 1 = индекс два. выводит последний идекс
*/
var c = [1,2,3];

console.log(c[c.length - 1]);

//Преобразование типа данных

var a = 9;
var c = 10.2;
var s = "a";

var b = a + "" + c + "aaa"; 
/*
Конкатенация - операция склеивания объектов линейной структуры, 
обычно строк. Если строку добавить к числу, получаем автоматически строку. 
*/
console.log(b);

var a = 9;
var c = 10.2;
var s = "98";
var b = "1";

var e = parseInt(10.78); //преобразует в целое значение 
console.log(e);

var  s1 = parseInt(s + b);
console.log(s1); // суммируются как строки
console.log(parseInt(s) + parseInt(b)); // арифметически складываются как числа

//while(1){} преобразование в тип булиан 


//Функция в данно случае выполняет вывод 100 в количестве вызовов функции

function show(){
    console.log(100);
}
show();
show();
show();
// можно вызывать функцию из функции

function f(){
    show();
}

for(var i = 0; i < 10; i++){
    f();
}

function div(a,b){
    console.log(a + " - " + b);
}
//нужно передать параметры в функцию, каждый раз могут использоваться др значения
div(10, 5);
div(2, 3);
div(11, -5);
div(130, 25);

function del(a,b){
    var r = a/b;
    console.log("r: " + r);
}
del(10, 5);
del(2, 3);
del(11, -5);
del(130, 25);

//return возвращает функцию

function r(){
    return 5;
}
var a = r();
console.log(a);


function delsum(a,b){
    var r = a / b;
    return r + b;
}

var r = delsum(delsum(12, 2), 2); //delsum(12, 2); delsum(8, 2);
/* Вложенность передачи данных функции, сначала функция возьмет значения
delsum(12, 2), посчитает их r = 8, и дальше передаст значение delsum(8, 2)
*/
console.log(r);

function sum(m){
    var res = 0;
    for( var i = 0; i < m.length; i++){
        res += m[i];
    }
    return res;
}

var r = sum([1,2,3,4,5,6,7,8,9]);
var mas = [1,2,3,4,-5,6,7,8,9];
var r1 = sum(mas); //35
var r2 = sum([0,-1,-1,-5]); //-7

console.log(r);
console.log(r1);
console.log(r2);

//Найти максимальное значение в массиве

function max(m){ //[присваивается рандомно заполненный масив]
    var a = m[0];
    for(var i = 1; i < m.length; i++){
        if(a < m[i]){ 
            a = m[i]; 
        }
    }
    return a;
}
//console.log(a) если вывести а, то var res = max(m) будет ненайден

//После функции мы пропписываем массив

var m = [];

for(var i = 0; i < 10; i++){
    m[i] = parseInt(Math.random() * 100);// целые случайные значения от 0 до 100
}
console.log(m);

var res = max(m);

console.log(res);

//Найти максимальное значение с множества массивов и записать в один массив

function maxMax(m){//перебирается заданное кол-во моссивов на макс значение
    //[91, 25, 29, 60, 68]
    var a = m[0]; // а равняется первому элементу массива
    for(var i = 1; i < m.length; i++){
        if(a < m[i]){ // в данном примере условие не выполнится и вернется a = 91
            a = m[i]; 
        }
    }
    return a;
}

var m = []; //задаем пустой массив для функции maxMax(m)
for(var i = 0; i < 4; i++){ // тут i кол-во массивов в массиве
    m[i] = []; //добавляем в массив  m[i] пустой массив
    for (var j = 0; j < 5; j++) { //тут j длина массивов
        m[i][j] = parseInt(Math.random() * 100); //заполнение массивов рандомными чисталми от 0 до 100
    }
}

var masMax = []; //задаем пстой массив для максимальных значений их всех массивов

for(var i = 0; i < m.length; i++){//бегает по пустому массиву для записи макс значений
    masMax[i] = maxMax(m[i]);//записывает максимальное значение найденное функцией maxMax(m), в данном случае первое = 91 и так далее, исходя из значений след-х массивов
}

var maxMax = maxMax(masMax); //задаем переменную для максимального значения в массиве из максимальных значений рандомных массивов

console.log(m);
console.log(masMax);
console.log(maxMax);

//Та же функция, только пустые массивы заполняет функция generateMas(n, k)
 
function maxX(m){ 
    var a = m[0];
    for(var i = 1; i < m.length; i++){
        if(a < m[i]){
            a = m[i];
        }
    }

    return a;
}

function generateMas(n, k){
    var m = [];
    for(var i = 0; i < n; i++){
        m[i] = [];
        for(var j = 0; j < k; j++){
            m[i][j] = parseInt(Math.random() * 100);
        }
    }

    return m;
}


var m = generateMas(2, 5);//передает значения (n, k) в функицю generateMas

var masMax = [];
for(var i = 0; i < m.length; i++){
    masMax[i] = maxX(m[i]);
}

var maxMax = maxX(masMax);

console.log(m);
console.log(masMax);
console.log(maxMax);

//Вызов функции через функцию

function f(){
    console.log(3);
}

function f2(){
    return f;
}

var a = f2();
a(); //оператор вызова функции


//Вызов функции через присваивание ее к переменной

function p(a){
    return a + 1;
}

var b = p(112);
console.log(b);

//Способ использования анонимной функции через переменную

var generate = function(n , k){ //создана функция без имени, присвоена переменной generate, теперь она и является функцией
    var m = [];
    for(var i = 0; i < n; i++){
        m[i] = [];
        for(var j = 0; j < k; j++){
            m[i][j] = parseInt(Math.random() * 100);
        }
    }

    return m;
}

var mas = generate(5,7);
console.log(mas);

//Передаем параметры в функцию 

function f3(){
    return 3;
}

function f4(){
    return 2;
}

function f5(a, b){ //Функция с параметрами называется высшим функционалом 
    return a() + b();//подставляет значения а и b, тем самым вызывая функции f3() и f4()
}

var i = f5(f3, f4); // Передает значения в функцию f5, где а = f3 b = f4
console.log(i); // 3 + 2 = 5

//Более сложная передача параметров в функцию 

function f6(a){
    return a++; //5
}

function f7(a, b){
    return a - b; //9+6
}

function f8(a, b){
    return a(5) + b(12, 3) + b(7, 1);
}

var i = f8(f6, f7);

console.log(i);

var l = function(){
    console.log(10);
};
var d = function(){
    console.log(20);
};

var c = function(a1, b1){
    a1();
    b1();
    return 0;
}


console.log(c(l, d));

//Функция вызывает сама себя

(function f9(){
    console.log(222);
})();

//Вызываем самовыз функцию с вложенной функцией  с помощью переменной

var a = (function (){
    function f(){
        console.log(3 + 2);
    }
    return f; //функция возвращает функцию f()
})();
a(); 

//еще пример вызова такой функции

var b = (function (){
    function f(a){
        return a*a;
    }
    return f; //функция возвращает функцию f()
})();

console.log(b(4));

// либо можно вывести переменную, которая равна функции, через другую переменную

var b = (function (){
    function f(a){
        return a*a;
    }
    return f; //функция возвращает функцию f()
})();

var c = b(4);
console.log(c);

//в функции можно возвращать массивы, вкладывать несколько функций

var b = (function (){
    function f(a){
        return a + a;
    }
    function f2(b){
        return b * b;
    }
    return [f, f2]; 
})();

// если мы хотим вывести оба значения функций return [f, f2], тогда пробежимся по массиву

for (var i = 0; i < b.length; i++){
    var res = b[i](3);
    console.log(res);
}

//Ассоциативный массив

var student = {
    firstName: "Ivan",
    lastName: "Ivanov",
    course: 3,
    estimate: 4.4,
};

console.log(student); //обращение к значению через свойство, лтбо можно писать так student["estimate"], находит по значению переменной

/*
//Другой вариант поиска 
var student = {
    firstName: "Ivan",
    lastName: "Ivanov",
    course: 3,
    estimate: 4.4,
    a: 45,
};

var a ="estimate";
console.log(student[a]);
*/

var students = (function (){
    var result = [];
    function add(student){
        result.push(student); //Метод push() добавляет один или более элементов в конец массива и возвращает новую длину массива.
    }
    function print(){
        console.log(result);
    }
    function remove(firstName){
        for(var i = 0; i < result.length; i++){
            if(result[i].firstName === firstName){
                result.splice(i, 1); //splice функция удаления, i индекс удаляемого эл-та, 1 кол-во удаляемых эл-тов
                break;
            }
        }
    }
    function avgEstimate(){
        var sum = 0;
        for(var i = 0; i < result.length; i++){
            sum += result[i].estimate;
        }
        return sum / result.length;
    }
    function getStudentsByEstimate(){
        var res = [];
        var avg = avgEstimate();//считаем заранее ср оценку сту-ов, чтобы в условии цикла не считалось значение много раз
        for(var i = 0; i < result.length; i++){
            if(result[i].estimate > avg){
                res.push(result[i]);
            }
        }
        return res;
    }

    return {
        add: add,
        print: print,
        remove: remove,
        avgEstimate: avgEstimate,
        getStudentsByEstimate: getStudentsByEstimate,
    } 
})();

var st = {
    firstName: "Vanya",
    lastName: "Smirnov",
    course: 2,
    estimate: 4.2,
};
var st2 = {
    firstName: "Alex",
    lastName: "Smirnov",
    course: 5,
    estimate: 4.9,
};
var st3 = {
    firstName: "Vanya",
    lastName: "Smirnov",
    course: 5,
    estimate: 3,
};

students.add(st);
students.add(st2);
students.add(st3);

//students.remove("Vanya")

students.print();

var a = students.avgEstimate();
var stus = students.getStudentsByEstimate();
console.log("avg: " + a);
console.log(stus);

