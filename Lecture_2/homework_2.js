/*
Переменная содержит в себе строку. Вывести строку в обратном порядке.
*/

var a = "JavaScript";
var someText = "";
for (var i = a.length - 1; i > -1; i--) {
    someText += a[i];
}
console.log(someText);

/*
Переменная содержит в себе число. Написать скрипт который посчитает факториал этого числа.
*/

var num = 10;
var fuctorial = 1;
for (i = 1; i < num + 1; i++) {
  fuctorial = fuctorial * i;
}
console.log(fuctorial);

/*
Дано число - вывести первые N делителей этого числа нацело.
*/

var num = 999; 
var amountDivisor = 9; 
var n = 0; 
for (i = 1; amountDivisor > n; i++) {
  if (num % i === 0) {
    n++;
    console.log(i);
  }
  else if (i > num) {
    console.log("Function does not work!");
    break;
  }
}

/*
Найти сумму цифр числа которые кратны двум 
*/

var num = 7894235687123462;
var res = 0;
var numStr = num + "";
for (i = 0; i < numStr.length; i++) {
  if (numStr[i] % 2 == 0) {
    var preob = parseInt(numStr[i]);
    res += preob;
  }
}
console.log(res);

/*
Найти минимальное число которое больше 300 и нацело делиться на 17
*/

var divisor = 17;
var dividend = 300;
var end = 0;
for (i = dividend; end < 1; i++) {
  if (i % divisor == 0) {
    console.log(i);
    end++;
  }
}

/*
Заданы две переменные для двух целых чисел, найти максимальное общее значение которое нацело делит два заданных числа.
*/

var firstNum = 60; 
var secondNum = 30; 
var leastNum = firstNum; 
if (firstNum > secondNum) {
  leastNum = secondNum; 
}
for (i = leastNum; i > 0; i--) { 
  if (firstNum % i == 0 && secondNum % i == 0) {
    console.log(i);
    break;
  }
}
