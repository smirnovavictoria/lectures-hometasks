/*
Переменная содержит в себе строку. Вывести строку в обратном порядке.
*/
// var 1
var someText = "JavaScript";
var a = "";

for(var i = someText.length - 1; i > -1; i--){
    
  a += someText[i];
  
}
console.log(a);

// var 2 
var someText = "JavaScript";
var a = "";

for(var i = 0; i < someText.length; i++){
    a = someText[i] + a;
  
}
console.log(a);

/*
Переменная содержит в себе число. Написать скрипт который посчитает факториал этого числа.
*/

var num = 9;
var factorial = 1;

for(var i = 1; i <= num; i++){
    factorial *= i;
}
console.log(factorial);

/*
Дано число - вывести первые N делителей этого числа нацело.
*/

// Выводит все целители 
var dividend = 12;
var divider = 1;

for(var i = 1; i <= dividend; i++){
    if((dividend % divider) === 0){
        console.log(divider);
        divider++;
    }else {
        divider++;
    }
}

// Выводит заданное кол-во делителей
var dividend = 50;
var n = 5;
var numdivisors = 1;

for(var i = 1; i <= dividend; i++){
    if((dividend % i) === 0 && numdivisors <= n){
        numdivisors++;
        console.log(i);
    }
}

/* 
Найти сумму цифр числа которые кратны двум
*/

var numStr = "123456789"; 
var sum = 0;
var divisor = 2;

for (i = 0; i < numStr.length; i++){
    if(numStr[i] % divisor === 0){
        var num = Number(numStr[i]);
        sum += num;
    }
}
console.log(sum);

/*
Найти минимальное число которое больше 300 и нацело делиться на 17
*/

var min = 300;
var divisor = 17;

for(i = 301; i > min; i++){
    if(i % divisor === 0){
        console.log(i)
        break;
    }
}

/*
Заданы две переменные для двух целых чисел, найти максимальное общее значение которое нацело делит два заданных числа.
*/

var a = 60;
var b = 30; 
var maxdev = 0;

for(i = 0; i <= a && i <= b; i++){
    if(a % i === 0 && b % i === 0){
        maxdev = i;
    }
}
console.log(maxdev);