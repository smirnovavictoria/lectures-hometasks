var a = 1;
var b = 2;
var c = a++ - 2 - ++b - b-- - ++b - 1 + a + b + ++b + a++;
/* 
c = 1 - 2 - 3 - 3 - 3 - 1 +2 + 3+ 4+2 = 0
a = 3   
b = 4   
*/
console.log(a);
console.log(b);
console.log(c);

var d = 3;
var v = 4;
var n = --d - v++ + v + d + v-- + ++v - --d + d-- + d;
/* 
d = 0
v = 5
n = 2 - 4 + 5 + 2 + 5 + 5 - 1 + 1 + 0 = 15
*/
console.log(d);
console.log(v);
console.log(n);

/*
for (initialization; conditions; step){
    //body
}
*/

for(var i = 0; i < 3; i++){
    console.log(i);

}

var s = 0;
for(var i = 0; i < 10; i++){
    s += i; 
}
console.log(s); 
/* 
 +=  b += a; b = b + a; 
 -=  b -= a; b = b - a;
 *=  b *= a; b = b * a;
 /=  b /= a; b = b / a;
*/ 
//написать числа фибоначи

console.log(0);
console.log(1); 

var n = 5;
var preLast = 0;
var last = 1;

for(var i = 0; i < n - 2; i++){
    var newValue = preLast + last;
    preLast = last; 
    last = newValue;
    console.log(newValue);
}

/*
 while(condition){
     выполняется только тогда, когда условие тру
 }
*/
// от 1 до 9
var i = 1;
while(i < 10){
    console.log(i);
    i++;
}
//наоборот от 9 до 1
var i = 9;
 while(i >= 0){
     console.log(i);
     i--;
 }

 /*
 do {
     сначала выполняет, а потом проверет по условию. 
     всегда віполняется минимум 1 раз, в отличии от while
 }
 */
var i = 1;
do {
    console.log(i);
    i--;
} while(i > -1);

// массив это набор значений

var m = [10,7,8,9,10];
var a = m[2];

console.log(m);
console.dir(m);
console.log(a);

var m = [];
var n = 10;

m[0] = 1;
m[1] = -9;

console.log(m); //[1, -9]

for(var i = 0; i < n; i++){
    m[i] = i*2;
}

console.log(m);

for(var i = 0; i < n; i++){
    m[i] = i*i;
}

console.log(m);

//массив может состоять из разных типов данных 

var m = [9, "dfsdfsdf", 77, [8, 9, [],[],[4,5,"fff"]], 0, [1,2,3],];

for(var i = 0; i < m.length; i++){
    console.dir(m[i]);
}

//просуммировать все значения в массиве

var m = [0,1,2,3,4,5,6,7,8,9];

var s = 0;

for(var i = 0; i < m.length; i++){
    s += m[i]; // s = s + m[i]
}
console.log(s);

//просуммировать все положительные значения в массиве

var m = [0,1,-2,3,4,-5,6,-7,8,9];
var s = 0;

for(var i = 0; i < m.length; i++){
    if(m[i] > 0) {
        s += m[i]; // s = s + m[i]
    }
}
console.log(s);

//посчитать кол-во 0 в массиве

var m = [0,1,-2,3,4,0,-5,0,0,6,-7,8,9,"0"];
var s = 0;

for(var i = 0; i < m.length; i++){
    if(m[i] == 0){
        s++;
    }
}
console.log(s);

//найти максимальный элемент массива

var m = [0,1,-2,3,4,0,-5,0,0,6,-7,8,9,"0"];
var s = 0;

for(var i = 0; i < m.length; i++){
    if(s < m[i]){
        s = m[i];
    }
}
console.log(s);
 
// Найти второе минимальное значение 

var m = [0,1,-2,3,4,0,-5,0,0,6,-7,8,9,"0"];
var s = 0;
var preLast = 0;

for(var i = 0; i < m.length; i++){
    if(s > m[i]){
        preLast = s; //0 -2 -5
        s = m[i]; //-2 -5 -7
    }
}
console.log(preLast);

//Вывести с, если а кратно 3

var a = 9;
var b = 3;

var c = a % b;

if((a % b) === 0){
    console.log(c)
}

//Посчитать сумму значений, которые кратны 2 !!!!!!!!!

var m = [0,1,-2,3,4,0,-5,0,0,6,-7,8,9,"0"];
var s = 0;
var preLast = 0;

for(var i = 0; i < m.length; i++){
    if((m[i] % 2) === 0){
        s += m[i];
    }
}
console.log(s);
//

//Посчитать сумму положительных значений в массиве, которые кратны 2

var m = [0,1,-2,3,4,0,-5,0,0,6,-7,8,9,"0"];
var s = 0;
var preLast = 0;

for(var i = 0; i < m.length; i++){
    if((m[i] % 2) === 0 && m[i] > 0){
        s += m[i];
    }
}
console.log(s);
